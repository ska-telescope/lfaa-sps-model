function map = response_polar(pos,el,gain,npoints,freq,dfreq,cal_err,taper)
%% RESPONSE_POLAR
%
% Computes a response of the array described by pos
% for elevation el, with gain curve gain, on a grid of 
% +/- npoints in x and y, and maximum offset angle 
% +/- acos(0.7)
% frequency freq
% Eanch antenna has the gain curve offsetted by dfreq(n)
% 
% Syntax: map=response(pos,el,gain,npoints,freq,dfreq, cal_err, taper)
%
%% Input
% pos: xy antenna position in meters for antenna n: pos(n,:) in m.
% el: elevation (radians), pi/2 = zenith
% gain: gain vs frequency, starting at 10 MHz, every 4 kHz
% npoints: final map size. Map is (2*npoints+1) squared
% freq: Frequency in MHz
% dfreq(n): Offset in frequency for each antenna gain
% cal_err(n): Calibration error for antenna n, nominal = 1. 
% taper: in dB(power) at station edge. Gaussian taper is assumed
% 
%% Ouput
% map(2*npoints+1, 2*npoints+1) : complex (voltage) response of the array 
%

%% Author: 
% G. Comoretto
% INAF - OAA - Florence
% email address:  comore@arcetri.astro.it  
% June 2018; Last revision: 20/06/18
%

%% ------------- BEGIN CODE --------------
  npos  = size(pos,1);
  scale = 0.5*pi/npoints;  % size of map is +/- pi/2
  freq0 = 10;     % MHz - starting point for gain
  fstep = 250;    % points/MHz for gain
  c = 299.792457; % m*MHz
  rmax=16.5;      % Station radius
  %taper=4;       % dB at station border
  t=sqrt(log(10)*taper/20)/rmax; % used in exponential
  %rot=[sin(el), 0, cos(el);0, 1, 0];
  % compute antenna based params
  resp=gain(round((freq-freq0+dfreq)*fstep))-gain(round((freq-freq0)*fstep));
  resp=(10.^(resp/20)).*cal_err;
  %x=pos*rot;
  omegac=2*pi*freq/c;
  for n=1:npos
      %g(n)=cos(norm(pos(n,:))/rmax*pi/2);
      g(n)=exp(-(norm(pos(n,:))*t)^2);
  end
  yp=(-npoints:npoints)'*ones(1,2*npoints+1);
  xp=ones(2*npoints+1,1)*(-npoints:npoints);
  phi=atan2(yp,xp);
  theta=sqrt(xp.*xp+yp.*yp)*scale;
  rx=sin(theta).*cos(phi);
  ry=sin(theta).*sin(phi);
  r0=cos(el)*pos(:,1);
  prim=max(cos(theta),0); % Primary beam (dipole)
  %
  % Compute map as a function of antennas and sum together
  map = zeros(2*npoints+1);
  for n=1:npos
      phase=(pos(n,1)*rx-r0(n)+pos(n,2)*ry)*omegac;
      map = map+resp(n)*g(n)*exp(1i*phase).*prim;
  end
end