addpath TPM_model
addpath PFB_model

quant1=0;
quant2=0;
quant3=0;
RFI_amp=0.;
RFI_freq=0.;
n_ms=1000;
Na=2;

ampl=[11,14,17,20,23,26,29,32,35]/127;

parfor i=1:length(ampl)
  tpm_model_main(quant1,quant2,quant3, ampl(i), RFI_amp, RFI_freq, n_ms,Na);
end

quant1=8;

parfor i=1:length(ampl)
  tpm_model_main(quant1,quant2,quant3, ampl(i), RFI_amp, RFI_freq, n_ms,Na);
end
