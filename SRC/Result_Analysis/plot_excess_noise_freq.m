%% PLOT_EXCESS_NOISE_FREQ - Plot the added noise as a function of frequency
% Plot the excess noise as a function of frequency for a range of signal
% amplitudes
%
% Syntax:  plot_excess_noise_freq
%
%% Inputs:
% None : 
% need to load *.mat files 
%
%% Outputs:
% None : 
% plot the excess noise as a function of frequency
%
%%  Mat files:
%           - *.mat files from "tpm_model _beamforming.m",
%             e.g. "LFAA_2Ant_bit8-0-0_sigma_0.12_RFI_0_T_200ms.mat";

%% Author: S.Chiarucci
% INAF - OAA - Florence
% email address:  simchi@arcetri.astro.it
% June 2018; Last revision: 20/06/18

%% ------------- BEGIN CODE --------------
% close all
% clear

F0 = 800e6;
% Cut the edges (50-350)MHz
a = 6913;
b = 48337;

%Select a group of channels (50-350  MHz)
fac = factor(b-a+1);
nch = fac(end);
f = (300e6/nch).*[1:nch]+50e6;
f1 = (300e6/(b-a)).*[1:b-a+1]+50e6;

% Load the MAT files
% filename = {'LFAA_2Ant_bit8-12-8_sigma_0.12_RFI_0_T_200ms.mat',...
%             'LFAA_2Ant_bit8-12-8_sigma_0.15_RFI_0_T_200ms.mat',...
%             'LFAA_2Ant_bit8-12-8_sigma_0.18_RFI_0_T_200ms.mat',...
%             'LFAA_2Ant_bit8-12-8_sigma_0.22_RFI_0_T_200ms.mat',...
%             'LFAA_2Ant_bit8-12-8_sigma_0.25_RFI_0_T_200ms.mat',...
%             'LFAA_2Ant_bit8-12-8_sigma_0.3_RFI_0_T_200ms.mat'};
%         Load the MAT files%             
filename = {'LFAA_2Ant_bit8-0-0_sigma_11ADU_RFI_0_T_200ms.mat',...
            'LFAA_2Ant_bit8-0-0_sigma_14ADU_RFI_0_T_200ms.mat',...
            'LFAA_2Ant_bit8-0-0_sigma_17,5ADU_RFI_0_T_200ms.mat',...
            'LFAA_2Ant_bit8-0-0_sigma_20ADU_RFI_0_T_200ms.mat',...
            'LFAA_2Ant_bit8-0-0_sigma_23ADU_RFI_0_T_200ms.mat',...
            'LFAA_2Ant_bit8-0-0_sigma_26ADU_RFI_0_T_200ms.mat',...
            'LFAA_2Ant_bit8-0-0_sigma_29ADU_RFI_0_T_200ms.mat',...
            'LFAA_2Ant_bit8-0-0_sigma_32ADU_RFI_0_T_200ms.mat',...
            'LFAA_2Ant_bit8-0-0_sigma_35ADU_RFI_0_T_200ms.mat'};
% filename = {'LFAA_2Ant_bit8-12-8_sigma_11ADU_RFI_0_T_200ms.mat',...
%     'LFAA_2Ant_bit8-12-8_sigma_14ADU_RFI_0_T_200ms.mat',...
%     'LFAA_2Ant_bit8-12-8_sigma_17,5ADU_RFI_0_T_200ms.mat',...
%     'LFAA_2Ant_bit8-12-8_sigma_20ADU_RFI_0_T_200ms.mat',...
%     'LFAA_2Ant_bit8-12-8_sigma_23ADU_RFI_0_T_200ms.mat',...
%     'LFAA_2Ant_bit8-12-8_sigma_26ADU_RFI_0_T_200ms.mat',...
%     'LFAA_2Ant_bit8-12-8_sigma_29ADU_RFI_0_T_200ms.mat',...
%     'LFAA_2Ant_bit8-12-8_sigma_32ADU_RFI_0_T_200ms.mat',...
%     'LFAA_2Ant_bit8-12-8_sigma_35ADU_RFI_0_T_200ms.mat'};

for k = 1:length(filename)
    load (char(filename(k)));
        
    xcq = reshape(XCq1(a:b,2),[],nch);
    xc  = reshape(XC1(a:b,2),[],nch);
%     ratio = std(xcq-xc).^2 ./ std(xc).^2;
        qratio = var(xcq - xc);
        var_norm = var(xc);
        ratio = qratio./var_norm/2;
    figure(3)
    %smooth the noise to cancel the RFI contribution
    semilogy(f./1e6,smooth(ratio,1000));
    hold on
    
end

% set the ylim
% ylim([1e-5 1e-2]);

% Set the legend
legend('11 ADU','14 ADU','17,5 ADU','20 ADU','23 ADU','26 ADU','29 ADU','32 ADU','35 ADU')

xlabel('Frequency (MHz)');
ylabel('Excess noise');
grid on