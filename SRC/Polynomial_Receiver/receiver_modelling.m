%% RECEIVER_MODELLING - Polynomial receiver model
% It compares the HDs&IMDs evaluated by Genesis with the ones evaluated
% using the polynomial equation 
%
%% Files required
% Other m-files required: compute_2imp(rfi_in); compute_3imp(rfi_in);
%
% MAT-files required: RFI_HD_IMD_AAVS1.mat = Genesis IMD&HD products
%                     Pol_receiver_IMD.mat= Polynomial receiver IMD&HD products
%
%% Output: 
% 2 Plots:
% - the comparing between the two set of IMD&HD products 
% - the normalized residual
%
%% Author: S.Chiarucci
% INAF - OAA - Florence
% email address: simchi@arcetri.astro.it  
% Jan 2019 - Last revision: 05-03-2019

%% note
% The MATLAB app 'Polynomial_receiver.mlapp' allows us to equalize the IP3 
% and IP3 values in order to reduce the residual between the set two of measures

%% ------------- BEGIN CODE --------------

clear
load('RFI_HD_IMD_AAVS1.mat');
compute_2imp(rfi_in);
compute_3imp(rfi_in);
load('Pol_receiver_IMD.mat');

GaindB = 40.815;
IP2 = 3.983%4.683% #ok
IP3 = -5.921%-6.321% #ok
freq = [freq_ip2;freq_ip3];
amp = [amp_ip2-IP2; amp_ip3 - 2*IP3]+ GaindB;
[~,i] = sort(freq);
f = freq(i);
a =  10.^(amp(i,:)/10);
clear freq amp

%% Eliminate duplicates, summing together power for lines with same frequency
 j=1;
   k = max(f);
   for i = 1:k
    ind = find(f(:) == i);
    if  isempty(ind)
        continue
    end
    freq(j,1) = i; %#ok
    a1(j,1) = sum(a(ind(1):ind(end),1)); %#ok
    j=j+1;
   end
 amp = 10*log10(a1);

%% remove the RFI tones
for i =1:length(rfi_in)
      index = find(rfi_out(:,1) == rfi_in(i,1));
        if  isempty(index)
        continue
        end
      rfi_out(index,:) = []; %#ok
end

for i =1:length(rfi_in)
      index = find(freq(:) == rfi_in(i,1));
        if  isempty(index)
        continue
        end
      freq(index,:) = [];  
      amp(index,:) = [];
end
  
%% plot results
figure
set(gcf, 'Position',  [100, 100, 650, 550])
plot(freq,amp,'x',rfi_out(:,1),rfi_out(:,2),'square')
xlabel('Frequency (MHz)')
ylabel('Output level (dBm)')
legend('IMD&HD from Polynomial model','IMD&HD from Genesys')
annotation('textbox',...
    [0.694527845036317 0.714738510301111 0.212251815980629 0.0983201267828883],...
    'String',{['IP2 = ' num2str(IP2) ' dBm'],['IP3 = ' num2str(IP3) ' dBm']},...
    'LineStyle','none');
grid on
figure
set(gcf, 'Position',  [100, 100, 650, 550])
semilogy(freq, abs(amp - rfi_out(:,2)),'*')
xlabel('Frequency (MHz)')
ylabel('Residual (dB)')
grid on
rms(abs(amp - rfi_out(:,2))./abs(rfi_out(:,2)))