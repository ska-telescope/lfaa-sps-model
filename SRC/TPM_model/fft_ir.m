function spt = fft_ir(h, len)
%% FFT_IR - Frequency response of a filter
% Computes frequency response of filter with impulse response h
% Absolute response, no phase response
% Only positive frequencies (len/2 points) returned
%
% Syntax:  spt = fft_ir(h, len)
%
%% Inputs:
% h =	vector 1xn	    Impulse response 
% len =   scalar		Output size
%
%% Outputs:
% spt =  vector 1xlen	Amplitude frequency response (absolute value)
%

%% Author: G.Comoretto
% INAF - OAA - Florence
% email address: comore@arcetri.astro.it  
% June 2018; Last revision: 20/06/18

%% ------------- BEGIN CODE --------------
    h1=zeros(1,2*len);
    l1=size(h,2);
    if mod(l1,2) == 1
     l2=(l1-1)/2;
    else
      l2=l1/2;
    end
    h1(1,1:l2)=h(1,l2+1:end);
    h1(1,(2*len-l2+1):end)=h(1,1:l2);
    spt1=fft(h1);
    spt(1:(len))=abs(spt1(1:(len)));
end