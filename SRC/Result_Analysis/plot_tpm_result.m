function plot_tpm_result(filename,fig_title)
%% PLOT_TPM_RESULT - Plot the auto- and cross-correlation estimated by the tpm simulation.
%
% Syntax:  plot_tpm_result(filename,fig_title)
%
%% Inputs:
%    filename = *.matfile from the tpm simulation
%    fig_title = title of the figure
%
%% Outputs:
% None:
%     Plot the auto and cross correlation 
%
%% Example: 
%    tpm_plot_result("LFAA_2Ant_bit8-12-8_sigma_0.15_2RFI_20dB.mat",...
%                    "Power Spectrum with 20dB RFI")
%

%% Author: S.Chiarucci
% INAF - OAA - Florence
% email address:  simchi@arcetri.astro.it 
% June 2018; Last revision: 20/06/18

%% ------------- BEGIN CODE --------------
    % load the file
    load(filename)
    
    % Set the samples relative to the frequency limit 50-350 MHz
    a=6965;b=48336;
    len=length(XC1);
    n=0:len-1;
    N=n*F0/(len-1)/2; % convert x-axis in actual frequency
    N = N(a:b);
    
    % Auto and Cross-correlation 
    AC_An = XC1(a:b,1);
    AC_An_q = XCq1(a:b,1);
    XC_A0A1 = XC1(a:b,2);
    XC_A0A1q = XCq1(a:b,2);
        
    figure  
        auto = AC_An;
        auto_q = AC_An_q;
        cross = abs(XC_A0A1);
        cross_q = abs(XC_A0A1q);
        semilogy(N/1e6,auto,N/1e6,cross);     
        set(gca,'xlim',[0.5e2, 3.5e2])
        set(gca,'ylim',[1e-0, 2*max(auto)])
        xlabel 'Frequency (MHz)'
        ylabel 'Signal power spectrum'
        title(fig_title)
        grid
        legend 'Auto correl.' 'Cross correl.'

end