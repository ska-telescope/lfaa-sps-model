# LFAA_MODELING

The present file explains the directory structure, giving information about 
its content.

-- PFB_model folder contains functions and the mat files for the polyphase 
filter. These files are necessary to channelize the signal during the
signal processing.

-- Polynomial_Receiver folder contains the polynomial receiver model. 
It was used to validate the generation process of the intermodulation 
products performed during the input signal generation process.

-- Result_Analysis folder contains useful functions and scripts for 
the analysis of the simulated output signal, from the plot of beamformed 
signal spectra to the quantization noise.

-- RFI_use_cases folder contains 3 mat files for 3 different RFI use cases. 
During the parameter configuration process it is possible to set the 
desired RFI use cases.

-- Skala-AL directory contains all the information about antenna 
configuration and its electromagnetic behaviour.
It is structured in the following sub-directores
- Skala40_isolata: it contains the single polarization electromagnetic 
                   simulation for the isolated antenna SKALA 4.0;
- Skala41_isolata: it contains the electroamgnetic simulation for 
                   the isolated antenna SKALA 4.1. P1 and P2 are two 
                   orthogonal polarizations; 
- SKA-TEL-LFAA-0200052-FN-REP-1-LFAA Field Station Layout Coordinates: it 
contains antenna layout for all the 512 LFAA station.

-- SkyModel directory contains information about the sky as seen by a LFAA 
station. It is structured in the 3  sub-directores, each containg the data 
for 3 different scenarios. The desiderd sky model is set during the 
parameter configuration process. The daily sky folder contains a function
to evaluate the sky diffuse emission.

-- TPM_model folder contains functions to run the TMP simulations. 

In the main directory are included:
- TPM_Parameters_configurator matlab app
- TPM_quick_starter matlab app
- tpm_starter function (the script version of the app)
- LFAA_guide that explains how to use the TPM_Parameters_configurator and 
TPM_quick_starter app.
