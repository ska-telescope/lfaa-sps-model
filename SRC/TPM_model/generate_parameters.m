function generate_parameters()
%% GENERATE_PARAMETERS - Function to generate all parameter files
%
% It generates the *.mat file used in the tpm simulation.
% It cointains 6 structures:
%
% Antenna  -> number of antennas with their positions inside the array and 
% the latitude and the longitude of the array itself. Moreover the right
% ascension and the declination angle of the astronomical source.
% Beamformer ->  information for needed by beamforming.
% Calibration -> number of channels, the antenna gains vs frequency and 
% the Antenna beam vs freq.
% Generator -> the sampling frequency and the amplitude
% spectrum for sky and noise
% Polyphase -> parameters for the polyphase filter
% RFI -> information on the position of the RFI source 
%
% Syntax: generate_parameters()
%
% It is also possible using the "TPM_Parameters_configurator" app to
% generate the proper configuration parameters file.
%
%% OUTPUTS :
%    '*.mat' = {  Antenna, 
%                          Generator, 
%                          Polyphase,
%                          Calibration, 
%                          Beamformer,
%                          RFI}
%
%% Files required:
% Other files required: filt_27_1024_16b.dec
%
% See also: TPM_Parameters_configurator.mlapp

%% Author: S.Chiarucci, G.Comoretto
% INAF - OAA - Florence
% email address: comore@arcetri.astro.it  
% June 2019; Last revision: 20/06/19

% ------------- BEGIN CODE --------------
%% Antenna parameters
pattern = load('Skala4_1_IDS_P1.mat','Directivity');
k = physconst('Boltzmann'); % Boltzmann constant
Antenna.N_ant  = 8;     % Number of antennas
array = load('SKALow_coord.mat');% SKA-low station coordinates;
lay =1;
Antenna.Stationlayout = lay;
layout = csvread(['layout_' int2str(lay-1) '.csv']);
Antenna.pos = layout(1:Antenna.N_ant,:);
Antenna.StationLongitude = array.SKALowcoord.longitude(lay);
Antenna.StationLatitude = array.SKALowcoord.latitude(lay);
Antenna.Latitude    = 26.824;  % in degrees
Antenna.Longitude   = 1.16764e+02; % degrees
sid_rate_factor     = 1;     % For tracking at sideral rate
sid_rate = 360.0/(23*3600+56*60+4.09); % Sideral time rate, deg/s
Antenna.HA_rate    =  sid_rate*sid_rate_factor;  % in degrees.sec
% Load Scenario
load('1_scenario/Scenario1.mat','DEC','RA','Date','T','Diff')
Antenna.Source_DEC = DEC'; % in degrees
Antenna.Source_RA = RA';   % in degrees
[Antenna.Source_HA,Antenna.Source_Elevation,Antenna.Source_Azimuth] =...
RaDec2AzEl(Date,Antenna.Source_RA,Antenna.Source_DEC,Antenna.Longitude,Antenna.Latitude);

%% Beamformer parameters
Beamformer.Source_RA  = RA(2);  % in degrees
Beamformer.Source_DEC = DEC(2); % in degrees
[Beam_HA,Beam_Elevation,Beam_Azimuth] =...
    RaDec2AzEl(Date,Beamformer.Source_RA,Beamformer.Source_DEC,...
    Antenna.StationLongitude,Antenna.StationLatitude);
Beamformer.HA         = Beam_HA;  % in degrees
Beamformer.HA_rate    = Antenna.HA_rate;    % in degrees.sec

%% Generator Parameters
Sky_spectrum = zeros(32768,5); % Spectral density, in K
Noise_spectrum = zeros(32768,1);
solid_angle_512px = (1.9/180*pi)^2*pi;
Generator.Sample_freq = 800e6; % Hz - ADC sample frequency
Generator.filter_size = 32768; % Spectral points in computing filters
min_freq = 10e6;               % For signal. Minimum and max freq, in Hz
max_freq = 400e6;
freq_step=0.5*Generator.Sample_freq/(Generator.filter_size-1);
min_frq_idx = round(min_freq/freq_step);
max_frq_idx = round(max_freq/freq_step);
freq        = (0:(Generator.filter_size-1))'*freq_step;
frequency = 10:10:400;
gain1=load('GT&Tn Chain.txt','-ascii');
rfiband = 24400;
%load the RFI use cases
load('RFI_usecases_1.mat','rfitot','f3'); 
rfi = [f3 rfitot];
Diffuse = interp1(frequency*1e6,Diff,freq,'pchip',0) ;

freq_idx = min_frq_idx:max_frq_idx;
Tsky = interp1(frequency*1e6,(T)',freq,'pchip');
 for j = 1 : length(frequency)
    Dir(j,:) = pattern.Directivity(j).beam(Antenna.Source_Elevation,Antenna.Source_Azimuth);
end
direct = interp1(frequency*1e6,Dir, freq,'pchip',0);
Sky_spectrum(freq_idx,:) = Tsky(freq_idx,:) .* direct(freq_idx,:) .* solid_angle_512px/4/pi;            


sky_ref(1:82,1) = Diffuse(1:82);
sky_ref(83:length(Noise_spectrum),1) = 4.17 + 20 * (408e6./freq(83:end)).^2.75;
Noise_spectrum(1:4096) = Diffuse(1:4096)*0.1 + 40; % since below 50MHz is not reliable 
Noise_spectrum(4097:end) = pchip(gain1(42:end,1)*1e6,gain1(42:end,3), freq(4097:end));
Noise_spectrum = sky_ref + Noise_spectrum; 
rfi_amp = rfi(:,2)*1e-3/(k*rfiband);
idx_0 = length(freq)-(length(rfi_amp)-1);
Noise_spectrum(idx_0:end) = Noise_spectrum(idx_0:end) + rfi_amp;        

% Receiver complex gain
% Different for each antenna
lev_ADC = 17.5;     % desired ADC level, in ADC units
gain = zeros(Generator.filter_size, Antenna.N_ant); 
gain_avg = 10.^((spline(gain1(:,1)*1e6, gain1(:,2), freq))/10);
scale    = lev_ADC.^2./mean((sum(Sky_spectrum,2)+ Noise_spectrum).*gain_avg);
gain_avg = gain_avg*scale;
rng('shuffle')
% Phase gain error in range (-0.5:0.5) radians
phase = (rand(Antenna.N_ant,1) - 0.5);
% Gain error as a slope
gain_err = rand(Antenna.N_ant,2)*0.4-0.2;
for i = 1:Antenna.N_ant
    slope = (1 + gain_err(i,1)) + 3*gain_err(i,2)/Generator.filter_size *((-Generator.filter_size/2):(Generator.filter_size/2-1))';
    gain(:,i) = gain_avg .* slope *exp(1j*phase(i));   
end
for i = 1:5
    Generator.Filter_sky(:,:,i)  = sqrt(Sky_spectrum(:,i) .* gain);% Amplitude spectra : sources x filtersize x n.Antennas
end
    Generator.Filter_noise = sqrt(Noise_spectrum .* gain_avg);

%% Polyphase parameters
load('filt_27_1024_16b.dec','ascii')
Polyphase.Nc  = 1024;          % Number of frequency points in FFT
Polyphase.Num = 32;            % Oversampling factor: Numerator
Polyphase.Den = 27;            % Denominator
Polyphase.H2D = filt_27_1024_16b;  % Filter tap coefficients 
Polyphase.scale = 2^4;         % Scaling factor

%% Calibration parameters
% Derive gain from generator parameters
Calibration.Nc = Polyphase.Nc/2; % Number of coarse channels
freq_cal = (0:(Calibration.Nc-1))'/Calibration.Nc*Generator.Sample_freq*0.5;
cal_gain = zeros(Calibration.Nc, Antenna.N_ant);
for j = 1 : length(frequency)
    cal_beam(:,j) = pattern.Directivity(j).beam(Beam_Elevation,Beam_Azimuth);
end
cal_beam = interp1(frequency*1e6,cal_beam,freq_cal,'pchip',0);                
for i = 1:Antenna.N_ant
    cal_gain(:,i) = sqrt(spline(freq, gain(:,i), freq_cal)); 
end
Calibration.gain = (cal_gain .* cal_beam);  % Amplitude gain for each antenna, at CC freq

%% RFI Parameters
RFI.Source_Az = -10;
RFI.Source_Elev = 10;

%% Save all parameters; 
% Remember to change the name of the mat file acording to the tpm
% simulation you want to run!!!
save('Parameters.mat', 'Antenna', 'Generator', 'Polyphase', ...
    'Calibration', 'Beamformer','RFI');

end