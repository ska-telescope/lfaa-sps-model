function [HA_G,HA,El,Az] = RaDec2AzEl(Date,RA,DEC,long,lat)
%% RADEC2AZEL - Coordinate conversion
% Coordinate conversion from Rigth Ascension and Declination to
% Azimuth,Hour angle and Elevation
%
% Syntax: [HA_G,HA,El,Az] = RaDec2AzEl(Date,RA,DEC,long,lat)
%
%% INPUTS:
%  Date = Date of Observation
%  RA,DEC = Rigth Ascension and declination of the sky object
%  long,lat = longitude and latitude of the observer
%
%% OUTPUT:
%  HA_G = Hour angle of the object referred to the Greenwich meridian
%  HA = Hour nagle referred to the local merdidian
%  El = Elevation
%  Az = Azimuth
%
%% Other m-files required: 
%  juliandate.m

%% ------------- BEGIN CODE --------------
[yyyy, mm, dd, HH, MM, SS] = datevec(datenum(Date,'yyyy/mm/dd HH:MM:SS'));
JD = juliandate(yyyy,mm,dd,HH,MM,SS);
T_UT1 = (JD-2451545)./36525;
ThetaGMST = 67310.54841 + (876600*3600 + 8640184.812866).*T_UT1 ...
+ .093104.*(T_UT1.^2) - (6.2*10^-6).*(T_UT1.^3);
ThetaGMST = mod((mod(ThetaGMST,86400*(ThetaGMST./abs(ThetaGMST)))/240),360);
ThetaLST = ThetaGMST + long;

HA_G = mod(ThetaGMST - RA,360);
HA = mod(ThetaLST - RA,360);
El = asind(sind(lat).*sind(DEC)+cosd(lat).*cosd(DEC).*cosd(HA));
Az = mod(atan2(-sind(HA).*cosd(DEC)./cosd(El),(sind(DEC)-sind(El).*sind(lat))./...
(cosd(El).*cosd(lat))).*(180/pi),360); 
end

