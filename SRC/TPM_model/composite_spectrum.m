function x = composite_spectrum(samp, Analysis_par, norm, gain_m, filt_coeff)
%% COMPOSITE_SPECTRUM - Auto- Cross- correlates the signal  
%
% It evaluates the auto and the cross correlation, removing the antenna gain 
% contributions, the normalization factor and correcting for filter band 
% shape in the final power spectra.
%
% Syntax:  x = composite_spectrum(samp, Analysis_par, norm, gain_m, filt_coeff)
%
%% INPUTS:
% samp = signal matrix [samples x frequency channels x n. of antenna];
% Analysis_par = filter parameters;
% norm = normalization factor to come back to calibrated data;
% gain_m = mean amplitude gain for all antennas;
% filt_coeff = polyphase filter coeffcients;
%
%% OUTPUTS:
% x = matrix containing autocorrelation (1st column) spectra and
% crosscorrelation spectrum (2nd column) for the first antenna. 
%
%% Other m-files required:
% response_filter.m
%
% See also: TPM_MODEL_MAIN, TPM_MODEL_BEAMFORMING

%% Author: S.Chiarucci, G.Comoretto
% INAF - OAA - Florence
% email address:  simchi@arcetri.astro.it, comore@arcetri.astro.it  
% June 2018; Last revision: 14/09/18

%% ------------- BEGIN CODE --------------

%   save ('16ant.mat', 'samp', 'Analysis_par', 'norm', 'gain_m', 'filt_coeff');
%   pause
  [~, nch, nant] = size(samp);
  n_ovs = Analysis_par.n_ovs;
  nch2 = nch * n_ovs;
%   nchtot = nch2-n_ovs/2;
  h = Analysis_par.h; 
  nc = Analysis_par.nc;
  len_f = size(h,1);
  len_s = size(samp,1);
  niter = floor((len_s-len_f)/nc)+1;
  freq2 = (Analysis_par.n1-1:Analysis_par.n2-1)/Analysis_par.nc-0.5;
  freq_cal = freq2 * 2;
  spt = zeros(n_ovs, nch, nant);
  calibration = 1./response_filter(freq_cal,filt_coeff',1024,32/27)'.^2;
  scale_band = nch2/2; % Scale factor due to different band, to convert 
                       % back in K. Further factor 2 is due to x2 in PFB
for ich = 1 : nch
  i0 = 0;
  for iter = 1 : niter      
      x1f = fftshift(fft(sum(reshape(samp(i0+(1:len_f),ich,1).* h, nc, []),2),nc));
    for iant = 1:nant     
      x2f = fftshift(fft(sum(reshape(samp(i0+(1:len_f),ich,iant).* h, nc, []),2),nc));
      y = x1f.*conj(x2f);
      spt(:, ich, iant) = spt(:,ich,iant)+ y(Analysis_par.n1:Analysis_par.n2);
    end
     i0 = i0 + nc;
  end
   spt(:,ich,:) =  spt(:, ich, :)/niter;  
end

  f1 = 0:(nch-1);
  f2 = (0:(nch2-1))/n_ovs;
  cal_f2 = 1./spline(f1,gain_m,f2).^2;
  spt = (spt.*norm'.^2).*calibration *scale_band;
  x = squeeze(circshift(reshape(spt,[nch2,nant]),-n_ovs/2,1)).*cal_f2.';
%   x((nchtot+1):end,:) = [];
end