k=physconst('Boltzmann');
x0 = 10*log10(Generator.Filter_noise.^2*k*24400/1e-3);
x1= 10*log10(x_orig1/(93312*32768*2)/(17.5^2/10^(-2.7/10)));
x2= 10*log10(x_orig2/(93312*32768*2)/(17.5^2/10^(-2.7/10)));
freq = (0:55295)'*800e6/(55296)/2;
subplot(2,1,1)
plot(freq/1e6,x2);
hold on
plot(freq/1e6,x1);
f0= (0:32767)' *800e6/32768/2;
plot(f0/1e6,x0)
subplot(2,1,2)
diff = abs(x1-x2);
x1(x2>-175) = NaN;
x1(x1>-155) = NaN;
x1(diff<=0) = NaN;
plot(freq/1e6,x1)







