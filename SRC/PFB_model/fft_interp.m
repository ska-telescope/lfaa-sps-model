%%fft_interp.m
%
% Interpolates a function using FFT, zero padding and inverse FFT
% 
%
%% Inputs
%
% f    vector 1xn     function to be interpolated
% n    scalar         Output dimension. Must be even
%%Outputs
%
% a    vector 1xsize  interpolated function
% 
%% Description
% 
% Interpolates a function using a FFT, zero padding and inverse FFT
% Zero of the function is at midpont, i.e. point (n+1)/2
% 
%% Changes
%
%  Author            Date           Comments
%  ---------------   -----------    -------------------------------------------
%  G. Comoretto      04-Mar-2014    Original version
%
%------------------------------------------------------------------------------
%
%% Code
function a=fft_interp(f,n)
    d=size(f,2);
    F0=fft(ifftshift(f));
    % If input dimension is odd do a 1/2 channel offset
    if (mod(d,2) == 1)
        d1=(d+1)/2;
        F0(1,1:d1) = exp(i*(0:(d1-1))*pi/n).*F0(1,1:d1);
        F0(1,(d1+1):end) = exp(i*((-d1+1):-1)*pi/n).*F0(1,(d1+1):end);
        F1 = [F0(1,1:d1), zeros(1,n-d), F0(1,d1+1:end)];
    else % shift by 1/2 final channel up and by original 1/2 channel down
        d1=d/2;
        s = pi/n*(-n/d+1);
        F1 = [exp(i*(0:(d1))*s).*F0(1,1:(d1+1)), zeros(1,n-1-d), ...
              exp(i*(-d1:-1)*s).*F0(1,(d1+1):end)];        
    end
    a = 1*fftshift(ifft(F1));
 end 
