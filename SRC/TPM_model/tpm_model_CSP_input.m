function tpm_model_CSP_input(samples_bit,eq_bit,beam_bit,s1,n_ms,Na,IMDGain)
%% TPM_MODEL_CSP_INPUT - Tile Processing Module simulation 
%
% The aim of this MATLAB function is to provide an  realistic input to the
% CSP Low model in order to simulate the entire SKA-low signal processing
% chain.
%
% It performs a 'Running Simulation' of the Tile Processing 
% Module of the LFAA with the Frequency Beamforming Approach. In this
% simulation, we focus on the beamformed signal taking
% into account the whole station.
% The sky (or in-beam) spectrum is specified in the Generator structure
% inside FullStation_Parameters.mat.
%
% Each antenna has a complex response given in Antenna structure. 
% The receiver noise + incoherent RFI spectrum is given in the Generator 
% structure and it is common to all antennas. 
%
% All quantizations are optional and specified as function parameters.
% The output is stored in bin files.
% 
% Processing is performed as: 
% - Initial quantization
% - Polyphase filterbank
% - Equalization and quantization
% - Phase Delay Correction  
% - Calibration and 16-bit quantization
% - Beamforming and final 8-bit quatization 
%
% After the last quantization signals are saved as 8-bit integer, ready to
% be trasmitted to the CSP-low model.
%
% Syntax:
% tpm_model_CSP_input(samples_bit,eq_bit,beam_bit,s1,n_ms,Na,IMDGain)
%
%% INPUTS:
%   samples_bit = n. of bits used for the ADC quantization;
%   eq_bit = n. of bits used in the quantization after the equalization
%   stage ;
%   beam_bit = n. of bits used in the quantization after the calibration;
%   s1 = Signal amplitude normalized to the clipping level. Its values must be
%   ranged from 0.01 to 1;
%   n_ms = Integration time (ms);
%   Na = Nunber of antennas used in the simulation;
%   IMDGain = 1000s IMD sensibility factor
%
%% OUTPUTS:
%   CCxq_beam = Quantized bemformed signal, saved in 'CSP_input_real.bin' 
%   and 'CSP_input_imag.bin' (real and imaginary part);
%   ST = Time Stamp of the samples in CCxq_beam, saved in
%   'CSP_input_time.bin'
%   delayP = Station delay. It is used during the signal processing in the
%   CSP
%% Files required
% Other m-files required: tpm_LFAA_signal.m, quantization.m,
%                      pfb_real.m
%
% Most parameters are in fixed configuration files;
% MAT-files required: FullStation_Parameters.mat
%
% See also: TPM_STARTER,  TPM_MODEL_BEAMFORMING, TPM_MODEL_MAIN

%% Author: S.Chiarucci, G.Comoretto
% INAF - OAA - Florence
% email address:  simchi@arcetri.astro.it, comore@arcetri.astro.it  
% June 2018; Last revision: 20/02/20

% ------------- BEGIN CODE --------------
  %% initialize 
    rng('default');
    %% Simulation Specs  
%     Na=8;
    fprintf('Configuring parameters and signals...')
    load('FullStation_Parameters.mat',...
        'Antenna','Beamformer','Calibration','Generator','Polyphase');
    position = Antenna.pos(1:Na,:);
    % load latitude and longitude of the station and array center
    lat = Antenna.StationLatitude;
    lon = Antenna.StationLongitude;
    layout = Antenna.Stationlayout; 
    lat0 = Antenna.Arraylat;
    lon0 = Antenna.Arraylon;
%     lat = lat0;
%     lon = lon0;
%     layout = 0;
    % output filename
    filename1 =['CSP_input_real_'  num2str(layout) '.bin'];
    filename2 =['CSP_input_imag_'  num2str(layout) '.bin'];
    filename3 =['CSP_input_time_'  num2str(layout) '.bin'];
    filename4 =['CSP_delay_'       num2str(layout) '.bin'];
    
%%      
    %From Geodetic to ENU
    Radius = 6371000;
    [xEast,yNorth,zUp] = geo2enu(lat,lon,0,lat0,lon0,0,Radius);
    % Antenna Positions | The Distance vector in Cartesian Coordinates with 
    % respect to the Array Reference Point
    %the antenna coordinate are alredy in ENU ref
    Ant_pos = [position(1:Na,1), position(1:Na,2), zeros(Na,1)]; 
    
    % From ENU to ECEF
    d_Vec_Array = [-sind(lon0) -sind(lat0)*cosd(lon0) cosd(lat0)*cosd(lon0); ...
                   cosd(lon0)  -sind(lon0)*sind(lat0) cosd(lat0)*sind(lon0); ...
                               0           cosd(lat0)            sind(lat0)] ...
                  * [xEast yNorth zUp].';
         
    d_Vec = [-sind(lon) -sind(lat)*cosd(lon) cosd(lat)*cosd(lon); ...
              cosd(lon) -sind(lon)*sind(lat) cosd(lat)*sind(lon); ...
                      0           cosd(lat)            sind(lat)] *Ant_pos.';
    
    Nsources = size(Generator.Filter_sky,3);
    Generator.Filter_sky = Generator.Filter_sky(:,1:Na,1:Nsources);              
    Generator.Filter_noise = Generator.Filter_noise(:,1:Na);
    HA_rate = Antenna.HA_rate;
    HA0 = Antenna.Source_HA0(1:Nsources);
    Ns = Generator.filter_size;
    c = physconst('LightSpeed');
    F0 = Generator.Sample_freq;
    ovs = Polyphase.Num / Polyphase.Den;
    filt_coeff = Polyphase.H2D;
    Nseg = length(filt_coeff);
    Nc = Polyphase.Nc;
    F1 = F0 / Nc * ovs;
    beam_DEC = Beamformer.Source_DEC;
    beam_HA0 = Beamformer.HA0;
    gain = Calibration.gain(:,1:Na);
%     Generator.Filter_noise =zeros(Ns,Na);  

    %% 
    %Number of Samples per Input Sample-Block
    Ns_O = Ns*27*16;   
    %The Duration of a single frame
    TD  = Ns_O/F0; % In sec
    %The Duration Needed to Fill the Pipe-Lines 
    TOV = Nseg/F0;
    %The duration of 32k-samples
    T0 = Ns/F0;
    %Simulation total time
    T_s = n_ms *1E-3; %in sec
    %Number of iterations required to cover the Simulation Duration
    NItt = ceil(T_s/(TD+TOV));
    
    %Load the receiver polynomial parameters
    Oip2=49;
    Oip3=31;
    ip2_ADU2 = 10^(Oip2/10)* (17.5^2/10^(-2.7/10));
    ip3_ADU2 = 10^(Oip3/10)* (17.5^2/10^(-2.7/10));
    a2 = 1 / sqrt(ip2_ADU2)/sqrt(2);
    a3 = 4/3/ip3_ADU2/2;

     %% Quantization Specifications
    % ADC quantization
    if samples_bit==0
        enbl_samples = false; % set false(true) to disable (enable) the quantizer
        RMS_ADC_bit = ((8-1)/2) * s1;
    else
        enbl_samples = true;
        samples_levels = 2^samples_bit-1; 
        RMS_ADC_bit = ((samples_levels-1)/2) * s1;
    end
       % Equalizer quantiztion
    if eq_bit==0
        enbl_eq = false; % set false(true) to disable (enable) the quantizer
        RMS_eq = 2047 * 0.20*sqrt(2);
    else
        enbl_eq = true;
        eq_levels = 2^eq_bit-1; 
        RMS_eq = (eq_levels-1)/2 * 0.20*sqrt(2);
    end
    % final quantization
    if beam_bit==0
        enbl_beam = false; % set false(true) to disable (enable) the quantizer
    else
        enbl_beam = true;
        beam_levels = 2^beam_bit-1; 
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    Delay_Specs.d_Vec = d_Vec;
    Delay_Specs.d_Vec_Array = d_Vec_Array;
    Delay_Specs.HA_rate = HA_rate;
    Delay_Specs.F0 = F0;
    Delay_Specs.Filter_noise = Generator.Filter_noise;
    Delay_Specs.c = c;
    Delay_Specs.Ns = Generator.filter_size;
    Delay_Specs.Na = Na;
    Delay_Specs.layout = layout;

    Delay_Specs.HA0 = HA0;
    Delay_Specs.Source_DEC = Antenna.Source_DEC(1:Nsources); 
    Delay_Specs.Source_RA = Antenna.Source_RA(1:Nsources);
    Delay_Specs.Filter_sky = Generator.Filter_sky; 
    %Generate the first segment
    first_segment = tpm_LFAA_signal(Delay_Specs,Ns,0,Nsources);   
    first_segment(1:end-Nseg,:) = [];
    
    nTimes = floor((Ns_O)/ (Nc/2/ovs)/2)+1;

    RMS_x_CZ = 1;    
    %freeRAM
    clear Generator Antenna Calibration position
    
    for nItt = 1:NItt

        fprintf('Running the Iteration %d of %d\n',nItt,NItt);
          T_s = (nItt-1)*TD + (T0-TOV);  % In sec
          
        %% Generating Signal
        fprintf('Generating Random Signal Sequence...')
        tic;
        signal = tpm_LFAA_signal(Delay_Specs,Ns_O,T_s+TOV,Nsources); 
        x_Vec = [first_segment; signal]; % signal with N_filt\ + Ns_O samples
        clear signal
        first_segment = x_Vec(end-Nseg+1:end,:);
        if nItt == 1
            RMS_x_CZ = rms(x_Vec,1);
        end
        x_Vec  = x_Vec./RMS_x_CZ;
        
        %% Set the RMS, add intermodulation product and quantize the signal
        x_Vec  = x_Vec * RMS_ADC_bit;     
        fprintf('Adding HDs&IMDs... \n')
        x_Vec = x_Vec + ((x_Vec.^2 * a2) + (x_Vec.^3 * a3)) * IMDGain; 
   
        if enbl_samples
            x_Vec_q = quantization(x_Vec,samples_levels);
        else 
            x_Vec_q = x_Vec;
        end
        clear x_Vec
        toc;
        
        %% Polyphase filterbank
        fprintf('Processing the Signals with Coarse Channelizer...')
        tic; 
        CCxq = zeros(nTimes, Nc/2,Na);
        for i=1:Na 
            CCxq(:,:,i) = pfb_real(x_Vec_q(:,i),filt_coeff,Nc/2,ovs);
        end
        clear x_Vec_q
        CCxq(end,:,:) = [];
        [Nsample,~] = size(CCxq(:,:,1)); 
        toc;
        
        %% FFT equalizer   
        fprintf('Equalizing the Signals...')
        tic;
        % Compute normalization and calibration coefficients
        % norm_curve(Ncc,Na): average RMS amplitude level
        % fft_eq: bits to discard in FFT equalizer (same for all antennas)
        % cal_coef: coefficient to normalize output to (RMS_eq*2^11)
        % gain_mean: mean amplitude gain for all antennas
        % gain_corr(Ncc,Na): Gain correction to apply
        % denormalization: factor to come back to calibrated data 
        % norm_fact(Ncc,Na): Normalization/calibration factor applied
        %
        if nItt == 1
            norm_curve = squeeze(rms(CCxq,1));
            norm_mean  = mean(norm_curve,2);
            gain_tot = gain(:,1:Na) ./ RMS_x_CZ ; % amplitude gain
            
            %% gain parameter is not defined from 1 to 13 channel
            %this line is necessary in order to not have for non-defined
            %channels a NaN element in the final result
            gain_tot(abs(gain_tot) == 0) = eps+1i*eps;
            gain_tot_mean = mean(abs(gain_tot),2);
            % FFT equalization, in bits to shift
            fft_eq = ceil(log2(max(norm_curve,[],2)./RMS_eq));
            fft_eq(fft_eq < 0) = 0;
            
           % 2^11 * 2^(eq_bit-beam_bit)  =    2^7 for 16-bit quant 
            norm_fact =  round(RMS_eq .*2^11.*(gain_tot_mean./gain_tot.*...
                         2.^fft_eq ./norm_mean)) ./ (2^11 * 2^(eq_bit-beam_bit) );                                       
        end
        clear gain gain_tot
        % Rescaling at FFT output
        CCxq= CCxq./(2.^fft_eq');
        if enbl_eq
          for i = 1:Na
                CCxq(:,:,i) = quantization(CCxq(:,:,i),eq_levels); %0.10 - 0.20 RMS level
          end 
        end 
        toc;

        %% Phase Delay Correction  
        fprintf('Delay and Phase correction...')
        tic; 
        f =(F0/2^(10)) * (0: 2^(9) -1).';
        %Sample-Times for the Delayed/Advanced Sequence
        SI_Vec = transpose(int64(round((T_s)*F1).' + (0 : 1 : Nsample-1)));
        ST = double(SI_Vec)/F1;
        HA = beam_HA0 + ST*HA_rate; 
        %The Time variable Delay in sec for the Time Duration
        TVD_Vec = -( [cosd(beam_DEC)*cosd(HA),...
                    -cosd(beam_DEC)*sind(HA),...
                    sind(beam_DEC)*ones(Nsample,1)]*d_Vec/c);
                
       % Generate the station delay         
       delayP = -[cosd(beam_DEC)*cosd(HA),...
                   -cosd(beam_DEC)*sind(HA),...
                   sind(beam_DEC)*ones(Nsample,1)]*d_Vec_Array/c;
               
        % Applying delays 
        for z =1:Na
            Phase_sft = exp(-2j*pi*TVD_Vec(:,z).*f');%-
            CCxq(:,:,z) =(CCxq(:,:,z).* Phase_sft);
        end  
        clear Phase_sft TVD_Vec
        toc;  

        %% Calibration   
        fprintf('Calibration process...')
        tic; 
        for i =1:Na
            CCxq(:,:,i) = CCxq(:,:,i).*norm_fact(:,i).'; 
        end
        if enbl_beam
           for i = 1:Na
                CCxq(:,:,i) = quantization(CCxq(:,:,i),beam_levels);% 0.17 - 0.22 RMS level
           end 
        end
        toc;
        
        %% Beamforming
        fprintf('Beamforming...')
        tic
%         First sum
        for j=1:(Na/8)
            CCxq_b1(:,:,j) = sum(CCxq (:, :, (1 : 8) + 8*(j-1)),3);
        end
        clear CCxq
        % for a 8-12-16-12-bit quantization with RMS of ~ 0.14 - 0.22
        CCxq_b1 = CCxq_b1 ./ 2^7;
        CCxq_b1 = quantization(CCxq_b1,2^12-1);
        % Second sum
        CCxq_beam = sum(CCxq_b1,3);
        clear CCxq_b1
        toc
        
        %% Final quantization
        CCxq_beam = CCxq_beam./Na/(2);       % /2 to get a quantization with RMS of ~ 0.14 - 0.19
        CCxq_beam = int8(quantization(CCxq_beam,2^8-1));
        
       %% Saving frame
       fprintf('Saving the Results...')
       tic; 
       % The pad operation is included to start the simulation in the CSP from t=0
       if nItt == 1
           tstart = ST(1);
           pad = round(tstart*F1);
           CCxq_beam = [zeros(pad,512); CCxq_beam];
           ST = [(0:F1^-1:tstart)';ST];
           delayP =  [zeros(pad,1); delayP];
       end
       
       fileID = fopen(filename1,'a');
       fwrite(fileID,real(CCxq_beam(:,77)),'int8');
       fclose(fileID);

       fileID = fopen(filename2,'a');
       fwrite(fileID,imag(CCxq_beam(:,77)),'int8');
       fclose(fileID);

       fileID = fopen(filename3,'a');
       fwrite(fileID,ST,'double');
       fclose(fileID);
       
       fileID = fopen(filename4,'a');
       fwrite(fileID,delayP,'double');
       fclose(fileID);

       toc;
    end %end for
end %end function