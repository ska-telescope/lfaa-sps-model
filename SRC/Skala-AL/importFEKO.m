%% importFEKO - import data from FEKO simulation
%  
% From the FEKO file defined in 'filename' variable, the script saves in 
% a mat file the electric field component and the directivity. The saved
% data have channel x phi x theta dimension
%
%% Author: S.Chiarucci
% INAF - OAA - Florence
% email address:  simchi@arcetri.astro.it
% April 2019

%% ------------- BEGIN CODE --------------
clear
freq=(10:10:400);% MHz
theta=(0:1:90)'; % angular interval used by FEKO 
phi=(0:5:355)';
Z=376.73;%Impedance of free space

for i = 1: length(phi)
    azimuth = phi(i,1);
     %conversion to l,m coordinates
     l(i,:) = sind(theta) .* cosd(azimuth);
     m(i,:) = sind(theta) .* sind(azimuth);
end

Ntheta=length(theta);
Nphi=length(phi);
Npoint=Ntheta*Nphi;
F = struct;
Beam = struct;
Directivity = struct;

filename = 'TriangleWire_MewFeedingWire_supporti_FarField1.txt';
fileID = fopen(filename);
formatspec = '%f%f%f%f%f%f%f%f%f';

for j = 1:length(freq)
    dataCADFEKO = textscan(fileID, formatspec,Npoint,'CommentStyle',{'#'});
    F(j).chan   = freq(j);
    F(j).data   =  cell2mat(dataCADFEKO);
    Etheta = F(j).data(:,3) + 1i.* F(j).data(:,4); % electric field theta component
    Ephi   = F(j).data(:,5) + 1i.* F(j).data(:,6); %electric field phi component
    D      = 10.^(F(j).data(:,9)/10);                       % directivity
    F(j).Etheta = reshape(Etheta,Ntheta,Nphi).';
    F(j).Ephi   = reshape(Ephi,Ntheta,Nphi).';
    F(j).D      = reshape(D,Ntheta,Nphi).';
end

Etheta = permute(cat(3,F.Etheta),[3,1,2]); % channel x phi x theta
Ephi   = permute(cat(3,F.Ephi),[3,1,2]); 
D      = permute(cat(3,F.D),[3,1,2]); 

for j=1:length(freq)
    Ep = squeeze(Ephi(j,:,:));  %phi component
    Et = squeeze(Etheta(j,:,:));%theta component
    Dir = squeeze(D(j,:,:));    %directivity
    U = (abs(Et).^2 + abs(Ep).^2)/2/Z; % radiation pattern
    Directivity(j).beam = scatteredInterpolant(l(:),m(:),Dir(:));
    Beam(j).beam = scatteredInterpolant(l(:),m(:),U(:));
    Prad(j) = Beam(j).beam(0,0)/Directivity(j).beam(0,0)*4*pi;
end
save('Skala4_0_FEKO.mat', 'Beam','Directivity','Prad');
% save('Skala4_0_FEKO.mat','Etheta','Ephi','D');