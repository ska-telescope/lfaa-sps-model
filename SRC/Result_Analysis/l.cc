#include "mex.h"
#include <math.h>

double bvnd(double dh, double dk, double r );
double phid( double Z);
//
// Function to compute the L distribution
// L(x1, x2, rho)
// equal the integral of the normal bivariate distribution with corr. rho
// integrated for the two variables above the two given limits
//
void mexFunction(int nlhs, mxArray *plhs[],
		int nrhs, const mxArray *prhs[])
{
     if (nrhs != 3) 
	mexErrMsgIdAndTxt("MyToolbox:L:nrhs", 
                          "Three inputs required.");
     if (nlhs != 1) 
	mexErrMsgIdAndTxt("MyToolbox:L:nlhs", 
                          "One output required.");
//
// All elements are scalar
//
     if (!mxIsDouble(prhs[0]) || 
          mxIsComplex(prhs[0]) ||
          mxGetNumberOfElements(prhs[0])!=1 ||
         !mxIsDouble(prhs[1]) || 
          mxIsComplex(prhs[1]) ||
          mxGetNumberOfElements(prhs[1])!=1 ||
         !mxIsDouble(prhs[2]) || 
          mxIsComplex(prhs[2]) ||
          mxGetNumberOfElements(prhs[2])!=1 ) {
             mexErrMsgIdAndTxt("MyToolbox:L:notScalar",
                      "Input elements multiplier must be a scalar.");
      }
//
// Retrieve elements and call function
//
    double x1, x2, rho, p;
    x1 = mxGetScalar(prhs[0]);
    x2 = mxGetScalar(prhs[1]);
    rho = mxGetScalar(prhs[2]);
    p = bvnd(x1, x2, rho);
//
// Store the result and return
//
    plhs[0] = mxCreateDoubleScalar(p);
}

#define MAX(x,y) ((x)>(y)?(x):(y))
#define MIN(x,y) ((x)<(y)?(x):(y))
#define STAR2(x) ((x)*(x))

//------------------------------------------------------------------------------
double bvnd(double dh, double dk, double r )      
{
    const double ZERO=0.0, TWOPI=2.0*M_PI;
    const double SQRT_TWO_PI = sqrt(TWOPI);
/*      Gauss Legendre Points (X) and Weightsi (W): 
 *      First index selects number of points used in approximation: 
 *      3 cases are for N =  6, N = 10 , N = 20 
 */
    const static double X[3][10] =
	{
	    { -0.9324695142031522e+00,
	      -0.6612093864662647e+00,
	      -0.2386191860831970e+00 },
	    
	    { -0.9815606342467191e+00,
	      -0.9041172563704750e+00,
	      -0.7699026741943050e+00,
	      -0.5873179542866171e+00,
	      -0.3678314989981802e+00,
	      -0.1252334085114692e+00 },
	    
	    { -0.9931285991850949e+00,
	      -0.9639719272779138e+00,
	      -0.9122344282513259e+00,
	      -0.8391169718222188e+00,
	      -0.7463319064601508e+00,
	      -0.6360536807265150e+00,
	      -0.5108670019508271e+00,
	      -0.3737060887154196e+00,
	      -0.2277858511416451e+00,
	      -0.7652652113349733e-01} };
    
    const static double W[3][10] =
	{
	    { 0.1713244923791705e+00, 
	      0.3607615730481384e+00, 
	      0.4679139345726904e+00 },
	
	    { 0.4717533638651177e-01,
	      0.1069393259953183e+00,
	      0.1600783285433464e+00,
	      0.2031674267230659e+00,
	      0.2334925365383547e+00,
	      0.2491470458134029e+00 },
	    
	    { 0.1761400713915212e-01,
	      0.4060142980038694e-01,
	      0.6267204833410906e-01,
	      0.8327674157670475e-01,
	      0.1019301198172404e+00,
	      0.1181945319615184e+00,
	      0.1316886384491766e+00,
	      0.1420961093183821e+00,
	      0.1491729864726037e+00,
	      0.1527533871307259e+00 } };
    
    int I, IS, LG, NG;
    double AS, A, B, C, D, RS, XS, BVN;
    double SN, ASR, H, K, BS, HS, HK;
    double fabsr = fabs(r);

    if( fabsr < 0.3 )
    {
	NG = 0;
	LG = 3;
    }
    else if ( fabsr < 0.75 )
    {
	NG = 1;
	LG = 6;
    }
    else
    {
	NG = 2;
	LG = 10;
    }
    
    H = dh;
    K = dk;
    HK = H*K;
    BVN = 0.0;
    if( fabsr < 0.925 )
    {
	HS = ( H*H + K*K )/2.0;
	ASR = asin(r);
	for( I=0; I<LG; I++ )
	{
	    for( IS=-1; IS<=1; IS+=2 )
	    {
		SN = sin( ASR*(IS*X[NG][I] + 1.0 )/2.0 );
		BVN += W[NG][I]*exp( ( SN*HK - HS )/( 1.0 - SN*SN ));
	    }
	}
	BVN = BVN*ASR/( 2.0*TWOPI ) + phid(-H)*phid(-K);
    }
    else
    {
	if( r < 0.0 )
	{
	    K = -K;
	    HK = -HK;
	}
	if( fabsr < 1.0 )
	{
	    AS = ( 1.0 - r )*( 1.0 + r );
	    A = sqrt(AS);
	    BS = STAR2( H - K );
	    C = ( 4.0 - HK )/8.0;
	    D = ( 12.0 - HK )/16.0;
	    ASR = -( BS/AS + HK )/2.0;
	    if ( ASR > -100.0 )
	    {
		BVN = A*exp(ASR)*( 1.0 - C*( BS - AS )*( 1.0 - D*BS/5.0 )/3.0 
				   + C*D*AS*AS/5.0 );
	    }
	    if ( -HK < 100.0 )
	    {
		B = sqrt(BS);
		BVN -= exp( -HK/2.0 )*SQRT_TWO_PI*phid(-B/A)*B *
		    ( 1.0 - C*BS*( 1.0 - D*BS/5.0 )/3.0 );
	    }
	    A = A/2.0;
	    for( I=0; I<LG; I++ )
	    {
		for( IS=-1; IS<=1; IS+=2 )
		{
		    XS = STAR2( A*( IS*X[NG][I] + 1.0 ) );
		    RS = sqrt( 1.0 - XS );
		    ASR = -( BS/XS + HK )/2.0;
		    if ( ASR > -100.0 )
		    {
			BVN = BVN + A*W[NG][I]*exp( ASR )
			    *( exp( -HK*( 1.0 - RS )/( 2.0*( 1.0 + RS ) ) )/RS        
			       - ( 1.0 + C*XS*( 1.0 + D*XS ) ) );
		    }
		}
	    }
	    BVN = -BVN/TWOPI;
	}
	if ( r > 0.0) 
	    BVN =  BVN + phid( -MAX( H, K ) );
	if ( r < 0.0 ) 
	    BVN = -BVN + MAX( ZERO, phid(-H) - phid(-K) );
    }
    return BVN;
}

//------------------------------------------------------------------------------
double phid( double Z)
{
    static const double 
	P0 = 220.2068679123761,
	P1 = 221.2135961699311, 
	P2 = 112.0792914978709,
	P3 = 33.91286607838300,
	P4 = 6.373962203531650,
	P5 = .7003830644436881,
	P6 = .03526249659989109;
    static const double 
	Q0 = 440.4137358247522,
	Q1 = 793.8265125199484, 
	Q2 = 637.3336333788311,
	Q3 = 296.5642487796737, 
	Q4 = 86.78073220294608,
	Q5 = 16.06417757920695, 
	Q6 = 1.755667163182642,
	Q7 = .08838834764831844;
    static const double 
	ROOTPI = 2.506628274631001,
	CUTOFF = 7.071067811865475;

    double EXPNTL, ZABS, P;
    
    ZABS = fabs(Z);
/*     
 *     |Z| &gt; 37
 */     
    if ( ZABS > 37.0 )
    {
	P = 0.0;
    }
    else
    {
/*     
 *     |Z| &lt;= 37
 */     
	EXPNTL = exp( -STAR2(ZABS)/2.0 );
/*     
 *     |Z| &lt; CUTOFF = 10/sqrt(2)
 */     
	if ( ZABS < CUTOFF )
	{
	    P = EXPNTL*( ( ( ( ( ( P6*ZABS + P5 )*ZABS + P4 )*ZABS
			       + P3 )*ZABS + P2 )*ZABS + P1 )*ZABS + P0 )
		/( ( ( ( ( ( ( Q7*ZABS + Q6 )*ZABS + Q5 )*ZABS 
			   + Q4 )*ZABS + Q3 )*ZABS + Q2 )*ZABS + Q1 )*ZABS + Q0 );
/*     
 *     |Z| &gt;= CUTOFF.
 */     
	}
	else
	{ 
	    P = EXPNTL/( ZABS + 1.0/( ZABS + 2.0/( ZABS + 3.0/( ZABS 
							  + 4.0/( ZABS + 0.65 ) ) ) ) )/ROOTPI;
	}
    }
    if ( Z > 0.0 )
	P = 1.0 - P;
    return(P);
}
