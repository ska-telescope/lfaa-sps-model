%% TPM_STARTER_2019 - It prepares and runs the tpm simulation
%  Here, you can set the following simulation parameters:
%
%  ampl = RMS signal amplitude  
%  n_ms = integration time
%  Na = number of antennas
%  RFI_dB = RFI tone amplitude(-s) (it may be an array)
%  RFI_freq = RFI tone frequency(-cies) (it may be an array)
%  quant1, quant2, quant3 = number of bit for the quantization stages
%
%  Last but not least, you can choose which tpm model you want to run:
% -tpm_model_main
% -tpm_model_CSP_input
% -tpm_model_beamforming
%
% -> To disable the RFI tone check the comment lines in the script;
% -> To improve the efficiency of the simulations, please take into account
% the oppurtunity of using the parfor command. For more information, check 
% examples below.
%
%% Example: 
%
% In this first example we have made and amp array to exploit the parfor. 
% In this way we run 4 simulation instances with 4 different amp values. 
% Pay attention to set the index 'i' in the right way. 
%
% [...]
% ampl = [0.15 0.2 0.25 0.3];
% parfor i=1:length(ampl)
%   tpm_model_main(quant1,quant2,quant3, ampl(i), n_ms,Na,IMDGain);
% end
%
% Same as above, but this time we run 3 simulation instances with 3
% different RFI tone amplitude values.
%
% We can use the parfor method with any of the parameters described above.
%
%% Other m-files required: 
%                         tpm_model_main.m, or
%                         tpm_model_CSP_input.m, or
%                         tpm_model_beamforming.m

%% Author: S.Chiarucci
% INAF - OAA - Florence
% email address:  simchi@arcetri.astro.it
% June 2019; Last revision: 20/09/19

%% ------------- BEGIN CODE --------------

close all
clear

addpath PFB_model
addpath TPM_model
addpath Result_Analysis
%%
% Select the number of bit for the quantization stages
quant1 = 8;
quant2 = 12;
quant3 = 8;
% Select Signal Amplitude
ampl = [0.1378];
% Select Time Simulation
n_ms =  200; %in msec
%Select the number of Antennas;
Na = 2;
%%--------------------------------------------------------------------

%% Select the RFI tone frequency(-cies)
RFI_freq = [155e6];

%% Select RFI tone Amplitude
RFI_dB = [10];   %RFI signal in decibel

% % RFI_amp is the sine wave amplitude
% % Psin = RFI_amp^2 / 2; -> power of the sine wave
RFI_sig = 10.^(RFI_dB/20) .* sqrt(2) .* (2.^8/2-1) *ampl; %#ok<*NASGU> for quant1 = 8;
% % Uncomment the following line to disable the RFI
RFI_sig = [0];  %#ok<*NBRAK>

RFI.rfi_freq = RFI_freq;
RFI.rfi_amp = RFI_sig;
RFI.azimuth = -10;
RFI.elevation = 10;

%save in configuration file according to the tpm simulation to run
save("Parameters.mat",'RFI','-append');
% save("Beam_Parameters.mat",'RFI','-append');
% save("FullStation_Parameters.mat",'RFI','-append');

%% intermodulation products
IMDGain = (1000/(n_ms*1e-3))^0.25; % to have the sensibilty of 1000s integration 
% IMDGain = 1;
%% ------------------------------------------------------------------------

for i=1:length(ampl)
    tpm_model_main(quant1,quant2,quant3,ampl(i), n_ms,Na,IMDGain);
%     tpm_model_beamforming(quant1,quant2,quant3,ampl(i), n_ms,Na,IMDGain)
%     tpm_model_CSP_input(quant1,quant2,quant3,ampl(i), n_ms,Na,IMDGain);
end