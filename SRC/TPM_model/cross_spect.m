function y = cross_spect(x1, x2, Analysis_par)
%% CROSS_SPECT - Cross correlate (or auto correlate when x1=x2) real signals
%
% Syntax:  y = cross_spect(x1, x2, Analysis_par)
%
%% INPUTS:
% x1 =  first signal
% x2 =  second signal 
% .h = filter coefficients
% .nc= number of channelsin the correlated signal
%% OUTPUTS:
%  y = cross correlation (or auto correaltion)

%% Author: G.Comoretto
% INAF - OAA - Florence
% email address: comore@arcetri.astro.it  
% June 2018; Last revision: 20/06/18

%% ------------- BEGIN CODE --------------
  h = Analysis_par.h;
  nc = Analysis_par.nc;
  len = size(x1,1);
  len_f = size(h,1); 
  y = zeros(nc,1);
  niter = floor((len-len_f)/nc)+1;
  i0=0;
  for i = 1:niter
      x1f = fftshift(fft(sum(reshape(x1(i0+(1:len_f)).* h, nc, []),2),nc));
      x2f = fftshift(fft(sum(reshape(x2(i0+(1:len_f)).* h, nc, []),2),nc));
      y = y + x1f.*conj(x2f);
      i0=i0+nc;
  end
end