function signal_delay = tpm_LFAA_signal(Delay_Specs,itt,Ts,Nsources)
%% TPM_LFAA_SIGNAL - Signal samples generator
%
% Generates a vector of samples with predefined spectrum,
% correlation function, and geometric delay.
%
% A total of itt samples are generated (rouded down to an 
% integer multiple of Ns), starting at time Ts.
%
% Syntax:  signal_delay = tpm_LFAA_signal(Delay_Specs,itt,Ts,Nsources)
%
%% INPUTS:
%  Delay_specs  - Structure with delay specifications. Elements:
%    .Filter_noise      Amplitude for noise+RFI (uncorrelated) component
%    .Filter_sky        Amplitude (complex) for sky (correlated) component
%    .Source_RA         RA, Dec coordinates for source
%    .Source_DEC                (degrees)
%    .HA, HA_rate       Sideral time (degrees) and sideral rate (deg/s)
%    .d_Vec             Antenna position, geocentric coord.(m), size(3, Na)
%    .c                 Speed of light = 299792458.0
%    .F0                Sampling frequency (samples/sec)
%    .Ns                Number of samples per segment (32768)
%    .Na                Number of antennas
%
%  itt          - number of samples: rounded to an integer multiple of Ns
%  Ts		- Initial time for 1st sample
%
% Filter_noise and Filter_sky specify the amplitude response in the frequency
%   domain, from frequency 0 to 0.5, and include the receiver response. 
% Filter_noise (size (Ns,1)) includes an averaged amplitude response
% Filter_sky (size (Ns, Na)) includes a complex response, one per antenna
% d_Vec contains antenna positions in a coordinate system with X->Greenwhich,
%	Y->East, Z->North pole, and referenced to the station delay centre
%
%% OUTPUT:
%  signal_delay - vector of (itt, Na) samples, one column per antenna
%
%  RMS amplitude of signal_delay is 
%    sqrt(mean(abs(Filter_noise).^2 + abs(Filter_sky).^2))
%
%
%% Other m-files required: 
%  tpm_LFAA_spect.m
%
% See also: TPM_MODEL_BEAMFORMING,TPM_MODEL_CSP_INPUT

%% Author: S.Chiarucci, G.Comoretto
% INAF - OAA - Florence
% email address:  simchi@arcetri.astro.it, comore@arcetri.astro.it  
% June 2018; Last revision: 20/06/18
% May 2019 added Nsources > 1
% June 2019 evaluate delay for the frame center (one point)

%% ------------- BEGIN CODE --------------
    DecA0       = Delay_Specs.Source_DEC;
    source_RA   = Delay_Specs.Source_RA;
    HA0         = Delay_Specs.HA0;
    HA_rate     = Delay_Specs.HA_rate;
    d_Vec       = Delay_Specs.d_Vec;
    d_Vec_Array = Delay_Specs.d_Vec_Array;
    c           = Delay_Specs.c;
    F0          = Delay_Specs.F0;
    Ns          = Delay_Specs.Ns;
    Na          = Delay_Specs.Na;
    persistent TVD_array
    f = fftshift((F0/2^(16)) * (-2^15: 2^(15) -1)).';
    Nitt = itt/Ns;
    signal_delay = zeros(Nitt*Ns,Na);
    Phase_sft = zeros(2*Ns,Na,length(source_RA));
    for k = 1 : Nitt
       fprintf('Running the frame %d of %d\n',k,Nitt);
        % Sample-Times for the Delayed/Advanced Sequence
        SI_Vec = transpose(int64(round(Ts*F0+(k-1)*2*Ns) + (0 : 2*Ns-1 : 2*Ns-1)));
        ST = mean(double(SI_Vec)/(F0));
        HA = HA0 + ST*HA_rate;
        % The Time variable Delay in sec for the Time Duration 
        % Added minus sign!!!12/12/19
        tvd1 = -[cosd(DecA0).*cosd(HA)...
                -cosd(DecA0).*sind(HA)...
                sind(DecA0).*ones(Nsources,length(ST))];    
        TVD_r =  (tvd1 * d_Vec_Array/c);
 
        if Nitt == 1 
            Delay_Specs.N_array = round(TVD_r * F0); 
            TVD_array = Delay_Specs.N_array/F0;
        end

         TVD_Vec =  -(TVD_r - TVD_array) + ( tvd1 * d_Vec/ c);
         
        % Applying delays  
        for i = 1:Na 
            Phase_sft(:,i,:) = exp(2j*pi*f.*TVD_Vec(:,i)');%+
        end
        
        if Nitt == 1
            signal_delay = tpm_LFAA_spect(Phase_sft,Delay_Specs);
        else
            signal_delay(((k-1)*Ns+1):((k)*Ns),:) = tpm_LFAA_spect(Phase_sft);
        end
    end
end %end function