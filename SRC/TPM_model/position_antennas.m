function antennas = position_antennas(N_ant, radius,dist)
%% POSITION_ANTENNAS -  Position N antennas in a circle of defined radius 
%
% Syntax:  antennas = position_antennas(N_ant, radius,dist)
%
%% Inputs:
% N_ant : Number of antennas
% radius: Circle radius in meters
% dist  : Minimum distance between antennas
%
%% Outputs:
% antennas: x-y position of the antennas
%
%% Example: 
% antennas = position_antennas(16, 17.5, 7.2)
% it generates an antenna ditribution in a circle of 17.5 m radius where the
% minimum distance between antennas is 7.2 meters
%
%% Author: G.Comoretto
% INAF - OAA - Florence
% email address: comore@arcetri.astro.it  
% June 2018; Last revision: 20/06/18

%% ------------- BEGIN CODE --------------
   i=1;
   radius2 = radius^2;
   dist2   = dist^2;
   antennas = zeros(N_ant,2);
   while i <= N_ant
     ant_pos = (rand(1,2)-0.5)*2*radius;
     if sum(ant_pos.^2) < radius2
       goodpos = 1;
       for j = 1:i
         if sum((ant_pos-antennas(j,:)).^2) < dist2
           goodpos = 0;
           end
       end
       if goodpos > 0
         antennas(i,:) = ant_pos;
         i = i+1;
       end
     end
   end
end