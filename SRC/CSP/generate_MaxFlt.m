function W = generate_MaxFlt(nbuff, nTap)
% Generate maximal flat filter coefficients

%{
% Author: J Bunton, 22 August 2015
Filter Response to meet correlator requirements
For a monochromatic signal, total power (all channels) remains constant
independent of frequency
Starting point are the maximally flat filters
this is improved with some simple optimisation

Calculation should be done only once per Simulink simulation  

nbuff = typically 4096
nTaps = # of taps, typically 8 or 12 

%} 

%{ 
SVN keywords
 $Rev:: 47                                                                                         $: Revision of last commit
 $Author:: bradford                                                                                $: Author of last commit
 $Date:: 2016-04-19 18:30:25 +1200 (Tue, 19 Apr 2016)                                              $: Date of last commit
 $LastChangedDate:: 2016-04-19 18:30:25 +1200 (Tue, 19 Apr 2016)                                   $: Date of last change
 $HeadURL: svn://codehostingbt.aut.ac.nz/svn/LOWCBF/Modelling/CSP_DSP/CSP_Dataflow/generate_MaxFl#$: Repo location
%}


%% Filter design coefficients 
% disp('generate_MaxFlt')

nTap2 = 2*nTap;  % say around 8 or 12 
nTap2p1 = nTap2+1; 
nTap2m1 = nTap2-1; 
nTap4 = 4*nTap;
imp=maxflat(nTap2,'sym',.5*nTap2/nTap2p1);
%imp=interpft(imp,nTap2)*nTap2p1/nTap2; %Take to 2*ntap (24) tap filter (2 channel, 12tap FIR)
imp = fft_interp2(imp,nTap4);
% plot(db(fft(imp)),'o-')
n1=nTap+1;
n2=3*nTap;
phaseshift = [exp(1j*(0:nTap)*pi/nTap4),zeros(1,nTap2m1),...
              exp(-1j*(nTap:-1:1)*pi/nTap4)];
% Interate to improve (hard coded 10 times) 
for k=1:10

    impf=fft(imp);
    imph=imp.*cos( ((1:length(imp))-1)*pi);
    impfh=fft(imph);
    impf((n1+1):n2)=0;
    impfh=[impfh(n1:nTap2p1),zeros(1,nTap2m1),impfh(nTap2p1:-1:(n1+1))];
    errorf =(impf.*conj(impf)+impfh.*conj(impfh));
    errorf=errorf/errorf(1);
    errorf=1-errorf;
    %errorf((n1+1):n2)=0;
    error=fftshift((ifft(errorf.*phaseshift)));
    % Force even symmetry
    error = error + fliplr(error);
    imp=imp+error/4; %/2.0*2 in symmetrization; %(2.5*( abs(impf)+abs(impfh) ));
    %
end

cor=imp;
corh=cor.*cos( ((1:length(imp))-1)*pi);

corf=freqz(cor,2048)*2048;
corfh=freqz(corh,2000)*2000;
ampf=corf.*conj(corf)+corfh.*conj(corfh);
error = fftshift((ifft(1-ampf)));

%{
Variable nbuff*nTap is the length of the filter. 
Typically: 4096 freq channels * 8 taps) 

12x32 is a 12 tap FIR section, 32 channel filterbank
%} 
%
W=interpft(cor,nbuff*nTap);  %change this line to alter length of filter.
W = W(:); % force column 
% Shift output in order to get symmetric odd filter. 
% Nominal time is at tap nbuff*nTap/2+1
%
nf1=nbuff*(nTap/2-1/8)+1;
nf2=nf1+nbuff*nTap/2-1;
W = [0;W(nf2: - 1:nf1);W((nf1 + 1):nf2)];

return 