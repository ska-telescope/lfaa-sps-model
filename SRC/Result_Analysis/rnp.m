function r=rnp(l1, l2, rho, n)
%% RNP - Computes the n-level correlation product for a bivariate distribution 
% Computes the n-level correlation product for a bivariate distribution 
% with correlation rho, quantized with n levels 
% with a quantization step of l1, l2 times the signal stdev. 
%
% Syntax:   r = rnp(l1, l2, rho, n)
%
%% Inputs:
% l1, l2 = quantization step in units of the distribution sigma
% rho    = correlation coefficiet of the distribution
% n      = Number of quantization levels
%
%% Constrains
%
% -1<rho<1
% n MUST BE EVEN
% for n odd use rn(l1,l2, rho,n)
%
%% Outputs:
% r= correlation product
%
% Other files required: l.mexa64 and l.cc
%
% See also: RN.m

%% Author: G.Comoretto
% INAF - OAA - Florence
% email address: comore@arcetri.astro.it  
% 20-Dic-14 

%% ------------- BEGIN CODE --------------

n2=n/2-1;
   n1=-n2;
   sum=-(n-1)*(n-1)/4;
   sqrt2=sqrt(0.5);
   for j = n1:n2
       lj2=l2*j;
       sum = sum + l(0,lj2,rho);
       for i=1:n2
           sum = sum + 2*l(l1*i, lj2, rho);
       end
   end
   for i=1:n2
       sum = sum + 0.5*(n-1)*erf(i*l1*sqrt2);
   end
   r=sum;
   return
end
% function r=rnp(l1, l2, rho, n)
%    n2=n/2-1;
%    n1=-n2;
%    sum=-(n-1)*(n-1);
%    sqrt2=sqrt(0.5);
%    for j = n1:n2
%        lj2=l2*j;
%        sum = sum + 4*l(0,lj2,rho);
%        for i=1:n2
%            sum = sum + 8*l(l1*i, lj2, rho);
%        end
%    end
%    for i=1:n2
%        sum = sum + 2*(n-1)*erf(i*l1*sqrt2);
%    end
%    r=sum;
%    return
% end
