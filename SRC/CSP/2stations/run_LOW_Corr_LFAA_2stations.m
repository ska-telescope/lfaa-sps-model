
%%
clear

layout = [5 512];
fileID = fopen(['CSP_input_time_' num2str(layout(end)) '.bin'],'r');
time = fread(fileID,'double');
fclose(fileID);
Ts = 1.08e-6; % [s] standard sample time for LOW

tdump = 0.25; % [s] approximate dump time (to the nearest frame*Ts). Note old value = 0.9
tstart = 0;
tfinal = 0.5; % [s] final simulation time 

CoarseSpacing = 25/32*1e6; % coarse channel spacing [Hz] (approx 781 kHz)
nFine_c_os = 4096 ;     % # of oversampled fine channels
nFine_c = nFine_c_os*27/32;     % # of fine channels = 3456
Fine_c = CoarseSpacing/nFine_c;  % [Hz] fine channel spacing 
   
N = 257;  % top coarse channel number (64 : 448, or 384 in total) 
fc = CoarseSpacing*(N-1) ;
wc = 2*pi*fc;              % coarse channel [rad/s]

% filter bank data 
nT = 8; % number of taps (also hardcoded at various points in embedded m-files)
Wcoeff = generate_MaxFlt(nFine_c_os,nT);

%% simulate Low_Correlator_S100
tic 
sim('LOW_Correlator_LFAA_2stations') % run the simulink model toc
toc 

%% Extract Sky signals & look at statitics 
Fc = LOW_dump.getElement('FineC').Values.Data;
cFc = LOW_dump.getElement('cFineC').Values.Data;
X = LOW_dump.getElement('Xvis').Values.Data;
tv =LOW_dump.getElement('Xvis').Values.Time;

%% Extract correlator output 

X = sum(X,3); % sum along time 
plot(db(abs(X),'power')); 
%  vline(M+1,'r--')
xlabel('frequency channel #')
