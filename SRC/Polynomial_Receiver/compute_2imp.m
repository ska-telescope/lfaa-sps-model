function [freq_ip2,amp_ip2]= compute_2imp(rfi_in)
%% compute_2imp - Compute the 2nd intermodulation products of a list of lines
%
% Input and output in dBm, for nominally IP2=0dB
% Output is sorted in order of increasing frequency, 
% with duplicates summed together
%
% Syntax:  [freq_ip2,amp_ip2]= compute_2imp(rfi_in)
%
%% Input
% rfi_in:  nx3 table of lines. 
%	Column 1 is frequency,
%	Column 2 is input amplitude (dB),
%	Column 3 is gain (dB)
%
%% Output: 
% freq_imp2: list of frequencies for the 2nd order intermodulation products
% amp_imp2:  list of amplitudes at input (dBm)
% 
%% Author: S.Chiarucci
% INAF - OAA - Florence
% email address: comore@arcetri.astro.it  
% Jan 2019; Last revision: 05-04-2019

%% note 
% the equation IMD2 = 2Prfi - IP2 
% replaces only the relation IMD2 = 20log(k2 * Ai Aj) 
% where k2 = 1/IP2 , A = Amax/sqrt(2) and Aj may be = Ai; ;

%% ------------- BEGIN CODE --------------

   k = size(rfi_in,1);
   freq=zeros(k);
   amp_i = zeros(k);
   
   for i = 1:k
     for j = 1:k
       if i < j
         freq(i,j) = abs(rfi_in(i,1) - rfi_in(j,1));
%          fprintf('f%i - f%i\n', i,j)
       else
         freq(i,j) = rfi_in(i,1) + rfi_in(j,1);
%          fprintf('f%i + f%i\n', i,j)
       end
       amp_i(i,j) = rfi_in(i,2) + rfi_in(j,2);
     end
     amp_i(i,i) = amp_i(i,i)-20*log10(2);
   end
   
   % Sort lines in order of increasing frequency
   freq = reshape(freq,[],1);
   amp(:,1) = reshape(amp_i,[],1);
   [~,i]=sort(freq);
   
   freq_ip2=freq(i);                      
   amp_ip2 = amp(i,:);
   
   filename = 'Pol_receiver_IMD.mat';
   save(filename,'freq_ip2','amp_ip2','-append');
end