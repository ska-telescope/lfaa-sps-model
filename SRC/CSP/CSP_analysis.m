%% CSP_analysis- It simulates the CSP signal processing chain
% These script simulates the CSP signal processing chain. It can be
% considered an faster alternative to the analogue simulink conuterpart. In
% order to work it needs as input the output bin file from
% "tpm_model_CSP_input". It accepts as input one channel of 2 different
% LFAA beamformed signal, although it can be easily modified to process
% more than 2 stations.
% 
%% Other m-files required: 
%                         generate_MaxFlt.m, or
%% Author: S.Chiarucci
% INAF - OAA - Florence
% email address:  simchi@arcetri.astro.it
% Oct 2019; Last revision: 20/02/20

%% ------------- BEGIN CODE --------------
clear
%% Set CSP simulation parameters

% Set the channel to process
N = 77;  % top coarse channel number (64 : 448, or 384 in total)
%Set the station to process (From 1 to 512) 
layout = [5 8];

CoarseSpacing = 25/32*1e6; % coarse channel spacing [Hz] (approx 781 kHz)
nFine_c_os = 4096 ;     % # of oversampled fine channels
nFine_c = nFine_c_os*27/32;     % # of fine channels = 3456
Ts = 1.08e-6; % [s] standard sample time for LOW 
fc = CoarseSpacing*(N-1) ;
Fine_c = CoarseSpacing/nFine_c;  % [Hz] fine channel spacing 
% filter bank data 
nT = 8; % number of taps (also hardcoded at various points in embedded m-files)
h = generate_MaxFlt(nFine_c_os,nT);
len_f = size(h,1);
m = [0:4096/2-1, -4096/2:-1]';

%% CSP Processing : First part
for j=1:length(layout)
    
    filename_r = ['CSP_input_real_' num2str(layout(j)) '.bin'];
    filename_i = ['CSP_input_imag_' num2str(layout(j)) '.bin'];
    filename_d = ['CSP_delay_'      num2str(layout(j)) '.bin'];

    fileID = fopen(filename_r,'r');
    pr = fread(fileID,'int8');
    fclose(fileID);
    fileID = fopen(filename_i,'r');
    pim = fread(fileID,'int8');
    fclose(fileID);
    s = pr+pim*1i;
    s= s(1:end);
    fileID = fopen(filename_d,'r');
    dd = fread(fileID,'double');
    fclose(fileID);
    d(:,j)=dd(1:end);
    
  % time domain delay correction
    samp(:,j) = s .* exp(2i*pi*d(:,j)*fc);
end

% Add 250 sample delay and shift the data stream aligning samples 
sampA = [zeros(len_f+250-round(d(end,1)./Ts),1); samp(:,1)];
dA = [zeros(250,1); d(:,1)];
sampB = [zeros(len_f+250-round(d(end,2)./Ts),1); samp(:,2)];
dB = [zeros(250,1); d(:,2)];

%% CSP Processing : Second part

% correct for the signal length when needed
% sampA = [];

samp = [sampA sampB];
[~, nstat] = size(samp);

% Set the channelizer parameters
nc=4096;
i0=0;
ich=1;
len_s = size(samp,1);
niter = floor((len_s-len_f)/nc)+1;
n1=321;
n2=4096-320;
spt = zeros(4096, niter);

dcorrA = round(dA(end)./Ts);
dcorrB = round(dB(end)./Ts); 

%% Channelization, fine channelize correction and correlation
for iter = 1 : niter
    x1f = fft( sum( reshape(samp(i0+(1:len_f),1).* h, nc, 8),2 ),nc ) ;
    x1f = x1f.*exp(2i*pi*m*Fine_c*(dA(1+2048*(iter-1)) - dcorrA*Ts));
for iant = 2:2 % change iant from 2:2 to 1:1 to plot autocorrelation 
    x2f = fft(sum(reshape(samp(i0+(1:len_f),iant).* h, nc, 8),2),nc);
    x2f = x2f.*exp(2i*pi*m*Fine_c*(dB(1+2048*(iter-1)) - dcorrB*Ts));
    y = x1f.*conj(x2f);
    spt(:,iter) =  y;
end
    i0 = i0 + nc;
end

%% plot the result

%Phase
figure;
spt=spt(:,10:end); %In the first 10 iterations the filter is not fully loaded
th1 = fftshift(mean(spt,2));
tha = angle(th1);
plot(smooth(tha(n1:n2),1)); 
%power  spectrum
figure;
plot(db(abs(th1(n1:n2,:)),'power'))
