%% PLOT_NOISE_ANALYSIS -
%
%
% Syntax:  plot_noise_analysis
%
%% Inputs:
% None
%
%% Outputs:
% None
%
% MAT-files required: LFAA_2Ant_bit8-0-0_sigma_0.15_RFI_0_T_200ms.mat,
%                     Parameters.mat
%

%% Author: G.Comoretto
% INAF - OAA - Florence
% email address: comore@arcetri.astro.it  
% June 2018; Last revision: 20/06/18

%% ------------- BEGIN CODE --------------

load('LFAA_2Ant_bit8-0-0_sigma_17,5ADU_RFI_0_T_200ms.mat');
load('Parameters.mat');
[N_tot, N_ant] = size(XC1);
f1=(0:(N_tot-1))/108;
f2=(0:32767)/64;
frq = f1*800e6/1024;
t = 32768/800e6*27*16*nItt;
df = 800e6/1024/108;
sigma = sqrt(1/t/df);
gain_mean = mean(abs(Calibration.gain),2);
gain_full=spline(0:511,gain_mean,(0:32767)/64).^2;
filter_sky = mean(abs(Generator.Filter_sky),2)'.^2;
filter_auto = (Generator.Filter_noise)'.^2+filter_sky;
filter_sky = filter_sky./gain_full/64*90;
filter_auto = filter_auto./gain_full/64*90;
sig_sky=spline(f2,filter_sky,f1);
sig_sky(sig_sky<1)=0;
jitter = [0.25 0.5 1]*1e-12;
for i=1:N_ant
  rad_noise(:,i) = smooth(std_xc1(:,i),128)./sig_sky'/sigma;
  quant_noise(:,i) = smooth(std_xcq1(:,i) - std_xc1(:,i),1024)./sig_sky'/sigma;
  quant_noise2(:,i) = sqrt(smooth(abs((XCq1(:,i)-XC1(:,i))).^2,128)*128)./sig_sky'/sigma;
end
jitter_noise = rad_noise(:,1).*(2*pi*frq'*jitter).^2;
spt_adc = mean(abs(Generator.Filter_sky),2)'.^2+(Generator.Filter_noise)'.^2;
jitter_amp = mean(spt_adc)*(45/19)^2*(350e6*jitter*2*pi).^2;
jitter_bbnoise = jitter_amp./gain_full'/64*90./filter_sky';
plot(frq/1e6,10*log10(rad_noise(:,1)-1),...
     frq/1e6,10*log10(abs(quant_noise(:,2))),...
     frq/1e6,10*log10(jitter_noise),...
     f2/1024*800,10*log10(jitter_bbnoise));
set(gca,'ylim',[-70 25]);
set(gca,'xlim',[50,350]);
grid
xlabel 'Frequency (MHz)';
ylabel 'Added noise (dB)';
legend 'Receiver + RFI' 'Quantization'...
    'nb jitter 250fs' '500fs' '1000fs'...
    'bb jitter 250fs' '500fs' '1000fs';
