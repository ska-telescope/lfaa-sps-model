function [resptot, resp2d, freq] = response_2step(h0, analysis)
%% RESPONSE_2STEP - Composite response of a 2 stage channelizer
%
% Given the filter tap coefficients for the first channelizer,
% and the characteristics of the second channelizer (number of channels,
% tap coefficients, first and last channels used)
% computes the cumulative response of the two stage oversampled
% channelizer, both for individual channels and for their integrated
% response
%
% Response is calculated for all fine channels used in a coarse channel, 
% and replicated to include one more fine channel on each side from the 
% next adjacent coarse channel
%
% Individual response for each fine channel is stored in a matrix. 
% To avoid memory overflow, the response is stored for a limited number of 
% fine channels, chosen to limit the total matrix size to less than 2^24 el.
% Response includes the two (or one) extra fine channels. 
%
%% Inputs:
% h0      = Tap coefficient vector, size (nx1). Normalization is irrelevant
% analysis = structure for 2nd channelizer
%   .h    = tap coefficient vector, size (nx1). Normalized to unity passband
%   .nc   = Number of channels in channelizer (FFT size)
%   .n1   = First channel in non oversampled region, range 0 :(nc-1) 
%   .n2   = Last channel in non oversampled region
%
% n1 and n2 can be semi-integer, for shifted FFT. In this case n1 = -n2+nc
% For conventional fft, usually n1 = -n2-1 + nc
% Frequency zero (DC) corresponds to channel nc/2
%
%% Outputs:
% resptot = Cumulative power response
% resp2d  = Power response for first individual channels
%           Size of the array is limited to 2^24 elements to avoid memory ovfl
% freq    = Frequency scale, normalized to the oversampled 
%           coarse channel width
%
%% Files required
% Other m-files required: response_filter.m, fft_ir.m

%% Author: 
% G. Comoretto
% INAF - OAA - Florence
% email address:  comore@arcetri.astro.it  
% June 2018; Last revision: 20/06/18
%

%% ------------- BEGIN CODE --------------

   % Computation parameters, extracted from analysis structure
   h1  = analysis.h';     % Impulse response for fine channelizer
   nf2 = 256;             % Plot points per fine channel
   nc = analysis.nc;      % Total number of channels
   n1 = analysis.n1-nc/2; % First and last fine channel 
   n2 = analysis.n2-nc/2; %
   nf1 = n2-n1+1;         % Number of used fine channels
   os = nc/nf1;           % Oversampling 
   % Derive other parameters
   % 
   nfreq = nc*nf2;        % Number of total computed frequency points
   freq = ((-nfreq/2):(nfreq/2-1))/nfreq; % Frequency scale
   fcent = (n1:n2)*nf2;   % Frequency at fine channel centre
   %
   % Power frequency response of the two channelizers
   % Coarse channelizer is computed by spline interpolation
   % Fine channelizer by FFT
   % Both responses are computed for ampl. and squared to obtain the power 
   %
   resp0 = response_filter(2*freq,h0',1024,os).^2; % Coarse chan. response 
   resp1 = fft_ir(h1,nfreq/2).^2; % Fine channelizer response (one sided)
   resp1b = [0,resp1((nfreq/2):-1:2),resp1(1:(nfreq/2))]; %   (two sided)
   % Normalization factor for each fine channel is inverse of the 
   % coarse channel response in the fine channel range
   norm = zeros(1,nf1);
   for i = 1:nf1
     norm(i) = 1.0./mean(resp0(fcent(i)+(((nfreq-nf2)/2):((nfreq+nf2)/2))));
   end
   % Plots just first fine channels, for a maximum array size of 2^24
   nplot = min(nf1,2^24/nfreq);
   % Allocate arrays
   resp2d  = zeros(nplot+2,nfreq);
   resptot = zeros(1,nfreq);
   % For each fine channel in the passband
   for i =1:nf1
     % shift the fine channel response to the right central frequency
     % and multiplies it for the coarse channel response
     resp = resp0.*circshift(resp1b,[0,fcent(i)])*norm(i);
     % individual response of each fine channel
     if i <= nplot
       resp2d(i+1,:) = resp;
     end
     % integrated response
     resptot = resptot +resp;
   end
   %
   % Add response of first and last filter to the frequency corresponding
   % to the next adjacent coarse channel   
   % Extra fine channel st right end is included in resp2d only if all
   % fine channels were also included. 
   %
   resp = circshift( ...
       resp0.*circshift(resp1b,[0,fcent(nf1)])*norm(nf1),[0,-nf1*nf2]);
   resp2d(1,:) = resp;
   resptot=resptot+resp;
   resp = circshift(...
       resp0.*circshift(resp1b,[0,fcent(1)])*norm(1),[0,nf1*nf2]);
   resptot = resptot +resp;
   if (nplot == nf1)
     resp2d(nplot+2,:) = resp;
   end
%end
