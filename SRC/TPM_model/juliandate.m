function jd = juliandate(year, month, day, hour, min, sec) 
%% JULIANDATE - Date Conversion
% Conversion from Gregorian to Julian date
%
% Syntax:  jd = juliandate(year, month, day, hour, min, sec) 
%
%% INPUTS:
%  year, month, day, hour, min, sec = Gregorian Date
%
%% OUTPUT:
%  JD = Julian Date

%% ------------- BEGIN CODE --------------

YearDur = 365.25;
for i = length(month):-1:1
    if (month(i)<=2)
        year(i)=year(i)-1;
        month(i)=month(i)+12;
    end
end
A = floor(YearDur*(year+4716));
B = floor(30.6001*(month+1));
C = 2;
D = floor(year/100);
E = floor(floor(year/100)*.25);
F = day-1524.5;
G = (hour+(min/60)+sec/3600)/24;
jd =A+B+C-D+E+F+G;

