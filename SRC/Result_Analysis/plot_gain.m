%% PLOT_GAIN - plot the gain and the gain error for auto and cross correlation 
% Plot the gain and the gain error for auto and cross correlation as  a
% function of the input signal level.
% It is measured in the portions of the spectrum without 
% significant RFI contribution
%
% Syntax:  plot_gain
%
%% Inputs:
% None : 
% need to load *.mat files 
%
%% Outputs:
% None : 
% plot the gain as a function of the input level
%
%% Files required: 
%  Mat files:
%           - *.mat files from "tpm_model _beamforming.m",
%             e.g. "LFAA_2Ant_bit8-0-0_sigma_0.12_RFI_0_T_200ms.mat";
%
% Other files required: corr_gain.m, l.mexa64 and l.cc

%% Author: S.Chiarucci
% INAF - OAA - Florence
% email address:  simchi@arcetri.astro.it 
% June 2018; Last revision: 20/06/18

%% ------------- BEGIN CODE --------------

% Load the *.mat file for different input signal amplitude
filename = {'LFAA_2Ant_bit8-0-0_sigma_0.12_RFI_0_T_200ms.mat',...
            'LFAA_2Ant_bit8-0-0_sigma_0.15_RFI_0_T_200ms.mat',...
            'LFAA_2Ant_bit8-0-0_sigma_0.18_RFI_0_T_200ms.mat',...
            'LFAA_2Ant_bit8-0-0_sigma_0.2_RFI_0_T_200ms.mat',...
            'LFAA_2Ant_bit8-0-0_sigma_0.22_RFI_0_T_200ms.mat',...
            'LFAA_2Ant_bit8-0-0_sigma_0.25_RFI_0_T_200ms.mat',...
            'LFAA_2Ant_bit8-0-0_sigma_0.3_RFI_0_T_200ms.mat',...
            'LFAA_2Ant_bit8-0-0_sigma_0.4_RFI_0_T_200ms.mat'};
% filename = {'LFAA_2Ant_bit8-0-0_sigma_11ADU_RFI_0_T_200ms.mat',...
%             'LFAA_2Ant_bit8-0-0_sigma_14ADU_RFI_0_T_200ms.mat',...
%             'LFAA_2Ant_bit8-0-0_sigma_17,5ADU_RFI_0_T_200ms.mat',...
%             'LFAA_2Ant_bit8-0-0_sigma_20ADU_RFI_0_T_200ms.mat',...
%             'LFAA_2Ant_bit8-0-0_sigma_23ADU_RFI_0_T_200ms.mat',...
%             'LFAA_2Ant_bit8-0-0_sigma_26ADU_RFI_0_T_200ms.mat'...
%             'LFAA_2Ant_bit8-0-0_sigma_29ADU_RFI_0_T_200ms.mat'...
%             'LFAA_2Ant_bit8-0-0_sigma_32ADU_RFI_0_T_200ms.mat'...
%             'LFAA_2Ant_bit8-0-0_sigma_35ADU_RFI_0_T_200ms.mat'};
    
% the sig vector must fit the input signal level in the mat files
sig=[0.12 0.15 0.18 0.2 0.22 0.25 0.3 0.4]';
% amp=[11 14 17.5 20 23 26 29 32 35]';
% sig = amp./(2^8/2-1);
% Select a region without RFI
a=4.165e4;
b=4.527e4;

for k = 1:length(filename)
    load (char(filename(k)));

    g_xc = mean(abs(XCq1(a:b,2)./XC1(a:b,2)));
    err_xc = std(abs(XCq1(a:b,2)./XC1(a:b,2)));
    err_xc = err_xc / sqrt(size(abs(XCq1(a:b,2)./XC1(a:b,2)),1));
    g_a0 = mean(real(XCq1(a:b,1)./XC1(a:b,1)));
    err_a0 = std(real(XCq1(a:b,1)./XC1(a:b,1)));
    err_a0 = err_a0 / sqrt(size(XCq1(a:b,1)./XC1(a:b,1),1));
    
    Gcross(k) = g_xc;
    Gauto(k) = g_a0;
    errcross(k) = err_xc;
    errauto(k) = err_a0;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Estimating the theoritical gain curve for Auto (rho=1) and 
%Cross(rho=0.001) correlation
% Choose the proper nbit value
lvl=0.01:0.01:1;
nbit = 2^8-1;
b=size(nbit);
rho=[0.001 1];
g=corr_gain(rho,nbit);

% Create figure
figure1 = figure('Color',[1 1 1]);
% % Create axes
axes1 = axes('Parent',figure1);
xlim(axes1,[0 1]);
ylim(axes1,[0 1.2]);
box(axes1,'on');
grid(axes1,'on');
hold(axes1,'all');

% Create xlabel
xlabel('Input Level (re peak digital)');
% Create ylabel
ylabel('Gain');
% Create title
title('Correlator gain for 4,5,6,7,8 and 9 bit quantiser');
errorbar(sig,Gcross,errcross,'o');
errorbar(sig,Gauto,errauto,'*');
hold on
plot1 = plot(lvl,(g(:,:)),'Parent',axes1,'LineStyle','--');
hold off

figure
errorbar(sig',(Gcross-spline(lvl,g(:,1),sig'))./Gcross*4.34,...
         errcross./Gcross*4.34,'*');
box('on');
grid('on');
% Create xlabel
xlabel('Input Level (re peak digital)');
ylabel('Gain error (dB)');