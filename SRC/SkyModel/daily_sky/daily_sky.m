clear

fileID1 = fopen('daily_sky.txt','r');
fileID2 = fopen('coord_sky.txt','r');
D = fscanf(fileID1, '%f', [24 1554]);
C = fscanf(fileID2, '%f', [2 1554])';
fclose('all');
load ('Skala4_1_IDS_P2.mat','Beam','Prad');

P_in = Prad(10)';
Date = ['2019/05/10 23:00:00'];
[~,el,az] = RaDec2AzEl(Date,C(:,1),C(:,2),116,-26);
    l = cosd(el) .* cosd(az);
    m = cosd(el) .* sind(az);
    U = Beam(10).beam(l,m);

solidangle = 0.00409;
Diff = sum((D(1:end-1,:) .* U') * solidangle,2)./P_in';