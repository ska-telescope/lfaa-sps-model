function x = composite_spectrum_pulsar(samp, norm, filt_coeff,nch)
%% COMPOSITE_SPECTRUM_PULSAR - Auto- Cross- correlates the signal  
%
% It evaluates the autocorrelation for the signal coming from
% the first antenna, removing the normalization factor and correcting for 
% filter band shape in the final power spectra.
% Only two channels with RFI are selected and channelized. 
% 
% Syntax:  x = composite_spectrum_pulsar(samp, norm, filt_coeff)
%
%% INPUTS:
% samp = signal matrix [samples x frequency channels x n. of antenna];
% norm = normalization factor to come back to calibrated data;
% filt_coeff = polyphase filter coeffcients;
% nch = Selected channel with RFI 
%% OUTPUTS:
% x = Autocorrelation of signal coming from the first antenna. Only two
% adjacent channels are taken into account in the calculation
%
%% Files required:
% Other m-files required: response_filter.m
%
% MAT-files required: Analysis_pss.mat
%
% See also: TPM_MODEL_PULSAR, COMPOSITE_SPECTRUM 

%% Author: S.Chiarucci, G.Comoretto
% INAF - OAA - Florence
% email address:  simchi@arcetri.astro.it, comore@arcetri.astro.it  
% June 2018; Last revision: 20/06/18

%% ------------- BEGIN CODE --------------
    % Select the corse channel with RFI
    nch1 = nch+1;
    % load the PSS filter : The CSP low PSS channelizer uses only 64 
    % channels per LFAA
    load('Analysis_pss.mat');
    [nsampl, ~, ~] = size(samp);
    nant = 1;
    chan = squeeze(samp(:,nch1:nch1+1,:));
    chans = chan.*exp(-2i*pi*((0:nsampl-1)+0.5)/128).';
    
    n_ovs = Analysis.n_ovs;
    nch2 = 2 * n_ovs;
    h = Analysis.h; 
    nc = Analysis.nc;
    len_f = size(h,1);
    len_s = size(chan,1);
    niter = floor((len_s-len_f)/nc)+1;
    freq2 = (Analysis.n1:Analysis.n2)/Analysis.nc-0.5;
    freq_cal = freq2 * 2;
    spt = zeros(n_ovs, 2, nant);
    calibration = 1./response_filter(freq_cal,filt_coeff',1024,32/27)'.^2;
    scale_band = nch2/2; % Scale factor due to different band, to convert 
                       % back in K. Further factor 2 is due to x2 in PFB
    % Evaluate the autocorrelation for the first antenna taking into account
    % only two channels (nch1, nch1+1)
    for iant = 1:nant
        for ich = 1:2
        i0 = 0;
            for iter = 1:niter      
                x1f = fftshift(fft(sum(reshape(chans(i0+(1:len_f),ich,iant).* h, nc, []),2),nc)); 
                x2f = fftshift(fft(sum(reshape(chans(i0+(1:len_f),ich,iant).* h, nc, []),2),nc));
                y = x1f.*conj(x2f);
                spt(:, ich, iant) = spt(:,ich,iant)+ y(Analysis.n1+0.5:Analysis.n2+0.5);
                i0 = i0 + nc;
            end
            spt(:,ich,:) =  spt(:, ich, :)/niter;
        end
    end
  spt = (spt.*norm(nch1:nch1+1)'.^2).*calibration * scale_band;
  x = squeeze(reshape(spt,[nch2,iant]));
%   semilogy(abs(x));
end
