function [map,rx,ry]=response_polar(pos,el,azimuth,npoints,freq,cal_err,taper)
%% [map,rx,ry]=response(pos,el,azimuth,gain,npoints,freq,dfreq, cal_err, taper)
%
% Computes a response of the array described by pos
% for elevation el, with gain curve gain, on a grid of 
% +/- npoints in x and y, and maximum offset angle 
% +/- acos(0.7)
% frequency freq
% Eanch antenna has the gain curve offsetted by dfreq(n)
% 
%% Input
% pos: xy antenna position in meters for antenna n: pos(n,:) in m.
% el: elevation (radians), pi/2 = zenith
% azimuth: azimuth in radians
% npoints: final map size. Map is (2*npoints+1) squared
% freq: Frequency in MHz
% cal_err(n): Calibration error for antenna n, nominal = 1. 
% taper: in dB(power) at station edge. Gaussian taper is assumed
% 
%% Ouput
% map(2*npoints+1, 2*npoints+1) : complex (voltage) response of the array 
%
%% Author: G.Comoretto
% INAF - OAA - Florence
% January 2019; Last revision: 14/02/19
%% Changes
% Added  azimuth input - S.Chiarucci

%% ------------- BEGIN CODE --------------
  npos  = size(pos,1);
  scale = 0.5*pi/npoints;  % size of map is +/- pi/2
  c = physconst('lightspeed');
  rmax=sqrt(max(pos(:,1).^2+pos(:,2).^2)); % Station radius
  t=sqrt(log(10)*taper/20)/rmax;           % used in exponential
  omegac=2*pi*freq/(c/1e6);
  % Tapering
  for n=1:npos
      g(n)=exp(-(norm(pos(n,:))*t)^2);
  end
  % Position grid
  yp=(-npoints:npoints)'*ones(1,2*npoints+1);
  xp=ones(2*npoints+1,1)*(-npoints:npoints);
  phi=atan2(yp,xp);
  theta=sqrt(xp.*xp+yp.*yp)*scale;
  rx=sin(theta).*cos(phi);
  ry=sin(theta).*sin(phi);
  
  r0x = (cos(el)*cos(azimuth)).*pos(:,1);
  r0y = (cos(el)*sin(azimuth)).*pos(:,2);
%   prim=max(cos(theta),0); % Primary beam (dipole)
  prim = 1; 
  % Compute map as a function of antennas and sum together
  map = zeros(2*npoints+1);
  for n=1:npos
      phase=(pos(n,1)*rx-r0x(n)+pos(n,2)*ry-r0y(n))*omegac;
      map = map+cal_err(n)*g(n)*exp(1i*phase).*prim;
  end
end