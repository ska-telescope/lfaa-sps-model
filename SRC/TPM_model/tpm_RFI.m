function  x_Tone = tpm_RFI(tone_freq, tone_ampl, Tau)
%% TPM_RFI - Generates a tone at frequency tone_freq (Hz), with amplitude tone_amp
%
% Syntax:  x_Tone = tpm_RFI(tone_freq, tone_ampl, Tau)
%
%% INPUTS:
% tone_freq   Tone frequency, in Hz
% tone_amp    Tone amplitude, absolute units
%
%% OUTPUTS:
%    x_Tone      Signal samples with the added tone 

%% Author: S.Chiarucci
% INAF - OAA - Florence
% email address:  simchi@arcetri.astro.it 
% June 2018; Last revision: 20/06/18

%% ------------- BEGIN CODE --------------
  [ns, na] = size(Tau);
  x_Tone = zeros(ns, na);
 
  for i = 1 : length(tone_freq)
      fprintf('Adding RFI %d of %d\n', i,length(tone_freq))
      
      if length(tone_ampl) == 1
         tone_w = tone_freq(i)*2.0*pi;
         x_Tone = x_Tone + tone_ampl* cos(tone_w* Tau);
      else
          integ = floor((tone_freq(i)* Tau));
          fract = (tone_freq(i)* Tau)-integ;
          x_Tone = x_Tone + tone_ampl(i)* cos(2.0 * pi * fract);
%         x_Tone = x_Tone + tone_ampl(i)* cos(tone_w* Tau);
      end
  end
