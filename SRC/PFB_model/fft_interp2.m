%%fft_interp.m
%
% Interpolates a function using FFT, zero padding and inverse FFT
% Preserves equiripple condition
% 
%
%% Inputs
%
% f    vector 1xn     function to be interpolated
% n    scalar         Output dimension. Must be even
%%Outputs
%
% a    vector 1xsize  interpolated function
% 
%% Description
% 
% Interpolates a function using a FFT, zero padding and inverse FFT
% Zero of the function is at midpont, i.e. point (n+1)/2
% 
%% Changes
%
%  Author            Date           Comments
%  ---------------   -----------    -------------------------------------------
%  G. Comoretto      04-Mar-2014    Original version
%
%------------------------------------------------------------------------------
%
%% Code
function a=fft_interp2(f,n)
  f0=f(1);
  fn=f(end);
  f(1)=f(2);
  f(end)=f(end-1);
  a=real(fft_interp(f,n));
  a(1)=f0*size(f,2)/n;
  a(end)=fn*size(f,2)/n;
end 
