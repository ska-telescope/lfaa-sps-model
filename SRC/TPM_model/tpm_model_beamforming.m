function tpm_model_beamforming(samples_bit,eq_bit,beam_bit,s1,n_ms,Na,IMDGain)
%% TPM_MODEL_BEAMFORMING - Tile Processing Module simulation 
%
% This MATLAB function performs a 'Running Simulation' of the Tile Processing 
% Module of the LFAA with the Frequency Beamforming Approach. In this
% simulation, we focus on the beamformed signal taking
% into account from 8 (half tile) to 256 antennas(16 tiles, in order to 
% simulate the whole station).
%
% The test vector is composed of a number of independently placed antennas 
% ranging from 8 to 256,
% pointing in a fixed direction in the sky. The sky spectrum
% is specified in the Generator structure inside Beam_Parameters.mat. 
%
% Each antenna has a complex response given in Antenna structure. 
% The receiver noise + incoherent RFI spectrum is given in the Generator 
% structure and it is common to all antennas. 
%
% A set of monochromatic lines directed towards a fixed position may be
% added to the whole signal. Line frequency and intensity is specified as a 
% function call parameter. 
% All quantizations are optional and specified as function parameters.
% The output is stored in a mat file.
% 
% Processing is performed as: 
% - Initial quantization
% - Polyphase filterbank
% - Equalization and quantization
% - Phase Delay Correction  
% - Calibration and 16-bit quantization
% - Frequency domain beamformer and 12-bit quantization
%
% After the last quantization signals are correlated and saved for analyzing
% purposes.
%
% Syntax:
% tpm_model_beamforming(samples_bit,eq_bit,beam_bit,s1,n_ms,Na,IMDGain)
%
%% INPUTS:
%   samples_bit = n. of bits used for the ADC quantization;
%   eq_bit = n. of bits used in the quantization after the equalization
%   stage ;
%   beam_bit = n. of bits used in the quantization after the calibration;
%   s1 = Signal amplitude normalized to the clipping level. Its values must be
%   ranged from 0.01 to 1;
%   n_ms = Integration time (ms);
%   Na = Nunber of antennas used in the simulation;
%   IMDGain = IMDGain = 1000s IMD sensibility factor
%
%% OUTPUTS:
%   XC2 = unquantized auto-(1st column) and cross-(2nd column) correlation 
%   as a function of frequency channels for the beamformed signal;
%   XCq2 = same as above but with quantization;
%   std_xc2, std_xcq2 = standard deviation for XC2 and XCq2;
%
%% Example 
%    If the function 
%    "tpm_model_beamforming(8, 12, 16, 0.2, 200, 256,IMDGain)"
%    is launched, we will get the output mat file
%    "Beam_LFAA_256Ant_bit8-12-16_sigma_0.2_RFI_0_T_200ms.mat"
%
%% Files required
% Other m-files required: tpm_LFAA_signal.m, tpm_RFI.m, quantization.m,
%                      cross_spect.m, pfb_real.m, composite_spectrum.m
%
% Most parameters are in fixed configuration files:
% MAT-files required: Beam_Parameters.mat, Analysis_128.mat
%
% See also: TPM_STARTER,  TPM_MODEL_CSP_INPUT, TPM_MODEL_MAIN

%% Author: S.Chiarucci, G.Comoretto
% INAF - OAA - Florence
% email address:  simchi@arcetri.astro.it, comore@arcetri.astro.it  
% June 2018; Last revision: 20/02/20

% ------------- BEGIN CODE --------------
    %% initialize 
    rng('default');
    %% Simulation Specs  
    fprintf('Configuring parameters and signals...')
    load('Beam_Parameters.mat',...
        'Antenna','Beamformer','Calibration','Generator','Polyphase','RFI');
    load('Analysis_128.mat', 'Analysis');
    position = Antenna.pos(1:Na,:);
    % load latitude and longitude of the station and array center
    lat = Antenna.StationLatitude;
    lon = Antenna.StationLongitude;
    layout = Antenna.Stationlayout; 
    lat0 = Antenna.Arraylat;
    lon0 = Antenna.Arraylon;
%     lat = lat0;
%     lon = lon0;
%     layout = 0;
%%
    %From Geodetic to ENU
    Radius = 6371000;
    [xEast,yNorth,zUp] = geo2enu(lat,lon,0,lat0,lon0,0,Radius);
    % Antenna Positions | The Distance vector in Cartesian Coordinates with 
    % respect to the Array Reference Point
    %the antenna coordinate are alredy in ENU ref
    Ant_pos = [position(1:Na,1), position(1:Na,2), zeros(Na,1)]; 
    
    % From ENU to ECEF
    d_Vec_Array = [-sind(lon0) -sind(lat0)*cosd(lon0) cosd(lat0)*cosd(lon0); ...
                   cosd(lon0)  -sind(lon0)*sind(lat0) cosd(lat0)*sind(lon0); ...
                               0           cosd(lat0)            sind(lat0)] ...
                  * [xEast yNorth zUp].';
         
    d_Vec = [-sind(lon) -sind(lat)*cosd(lon) cosd(lat)*cosd(lon); ...
              cosd(lon) -sind(lon)*sind(lat) cosd(lat)*sind(lon); ...
                      0           cosd(lat)            sind(lat)] *Ant_pos.';
    
    Nsources = size(Generator.Filter_sky,3);
    Generator.Filter_sky = Generator.Filter_sky(:,1:Na,1:Nsources);              
    Generator.Filter_noise = Generator.Filter_noise(:,1:Na);
    HA_rate = Antenna.HA_rate;
    HA0 = Antenna.Source_HA0(1:Nsources);
    Ns = Generator.filter_size;
    c = physconst('LightSpeed');
    F0 = Generator.Sample_freq;
    RFI_Az = RFI.azimuth;
    RFI_Elev = RFI.elevation;
    RFI_amp = RFI.rfi_amp;
    RFI_freq = RFI.rfi_freq;
    ovs = Polyphase.Num / Polyphase.Den;
    filt_coeff = Polyphase.H2D;
    Nseg = length(filt_coeff);
    Nc = Polyphase.Nc;
    F1 = F0 / Nc * ovs;
    beam_DEC = Beamformer.Source_DEC;
    beam_HA0 = Beamformer.HA0;
    gain = Calibration.gain(:,1:Na);
%     Generator.Filter_noise =zeros(Ns,Na);  
    %%
    %Number of Samples per Input Sample-Block
    Ns_O = Ns*27*16;   
    %The Duration of a single frame
    TD  = Ns_O/F0; % In sec
    %The Duration Needed to Fill the Pipe-Lines 
    TOV = Nseg/F0;
    %The duration of 32k-samples
    T0 = Ns/F0;
    %The duration of the simulation
    T_s = n_ms *1E-3; %in sec
    %Number of iterations required to cover the Simulation Duration
    NItt = ceil(T_s/(TD+TOV));
    
    %Load the receiver polynomial parameters
    Oip2=49;
    Oip3=31;
    ip2_ADU2 = 10^(Oip2/10)* (17.5^2/10^(-2.7/10));
    ip3_ADU2 = 10^(Oip3/10)* (17.5^2/10^(-2.7/10));
    a2 = 1 / sqrt(ip2_ADU2)/sqrt(2);
    a3 = 4/3/ip3_ADU2/2;
    
    %% Quantization Specifications
    % RFI stage
    if RFI_amp==0
        enbl_RFI = false;
    else
        enbl_RFI=true;
    end
    % ADC quantization
    if samples_bit==0
        enbl_samples = false; % set false(true) to disable (enable) the quantizer
        RMS_ADC_bit = ((8-1)/2) * s1;
    else
        enbl_samples = true;
        samples_levels = 2^samples_bit-1; 
        RMS_ADC_bit = ((samples_levels-1)/2) * s1;
    end 
       % Equalizer quantiztion
    if eq_bit==0
        enbl_eq = false; % set false(true) to disable (enable) the quantizer
        RMS_eq = 2047 * 0.20*sqrt(2);
    else
        enbl_eq = true;
        eq_levels = 2^eq_bit-1; 
        RMS_eq = (eq_levels-1)/2 * 0.20*sqrt(2);
    end
    % final quantization
    if beam_bit==0
        enbl_beam = false; % set false(true) to disable (enable) the quantizer
    else
        enbl_beam = true;
        beam_levels = 2^beam_bit-1; 
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    Delay_Specs.d_Vec = d_Vec;
    Delay_Specs.d_Vec_Array = d_Vec_Array;
    Delay_Specs.HA_rate = HA_rate;
    Delay_Specs.F0 = F0;
    Delay_Specs.Filter_noise = Generator.Filter_noise;
    Delay_Specs.c = c;
    Delay_Specs.Ns = Generator.filter_size;
    Delay_Specs.Na = Na;
    Delay_Specs.layout = layout;

    Delay_Specs.HA0 = HA0;
    Delay_Specs.Source_DEC = Antenna.Source_DEC(1:Nsources); 
    Delay_Specs.Source_RA = Antenna.Source_RA(1:Nsources);
    Delay_Specs.Filter_sky = Generator.Filter_sky; 
    %Generate the first segment
    first_segment= tpm_LFAA_signal(Delay_Specs,Ns,0,Nsources);   
    first_segment(1:end-Nseg,:) = [];
    
    nTimes = floor((Ns_O)/ (Nc/2/ovs)/2)+1;
%     N_tot = Nc/2*Analysis.n_ovs-Analysis.n_ovs/2;
    N_tot = Nc/2*Analysis.n_ovs;
    XC2     = zeros(N_tot,1);
    XCq2    = zeros(N_tot,1);
    XC2_v   = zeros(N_tot,1);
    XCq2_v  = zeros(N_tot,1);
    RMS_x_CZ = 1;
    
    %freeRAM
    clear Generator Antenna Calibration position
    
    for nItt = 1:NItt

        fprintf('Running the Iteration %d of %d\n',nItt,NItt);
          T_s = (nItt-1)*TD + (T0-TOV);  % In sec
          
        %% Generating Signal
        fprintf('Generating Random Signal Sequence...')
        tic;
        signal = tpm_LFAA_signal(Delay_Specs, Ns_O, T_s+TOV, Nsources); 
        x_Vec = [first_segment; signal]; % signal with N_filt\ + Ns_O samples
        clear signal
        first_segment = x_Vec(end-Nseg+1:end,:);
        if nItt == 1
            RMS_x_CZ = rms(x_Vec,1);
        end
        x_Vec  = x_Vec./RMS_x_CZ;
        
        %% Adding RFI signal: tone sine waveform
        if enbl_RFI 
            fprintf('Adding RFI signal... \n')
            
            tau = ones(size(x_Vec,1),1) .* ...
                                          [cosd(RFI_Elev)*sind(RFI_Az),...
                                           cosd(RFI_Elev)*cosd(RFI_Az),...
                                           sind(RFI_Elev)] * Ant_pos.'/c;
         
            %Sample-Times for the Delayed/Advanced Sequence
            SI_Vec = transpose(int64(round((T_s)*F0).' + ...
                                              (0 : 1 : size(x_Vec,1)-1)));
            ST = double(SI_Vec)/F0;
            RFI_sig  = tpm_RFI(RFI_freq,RFI_amp,ST-tau);
        end

     %% Set the RMS and add intermodulation product and quantize the signal
        x_Vec  = x_Vec * RMS_ADC_bit;     
        fprintf('Adding HDs&IMDs... \n')
        x_Vec = x_Vec + ((x_Vec.^2 * a2) + (x_Vec.^3 * a3)) * IMDGain; 
        
        if enbl_RFI 
            x_Vec = RFI_sig + x_Vec;
        end
        
        if enbl_samples
            x_Vec_q = quantization(x_Vec,samples_levels);
        else 
            x_Vec_q = x_Vec;
        end
        toc;
        
        %% Polyphase filterbank
        fprintf('Processing the Signals with Coarse Channelizer...')
        tic; 
        CCx = zeros(nTimes, Nc/2,Na);
        for i=1:Na 
            CCx(:,:,i)  = pfb_real(x_Vec(:,i),filt_coeff,Nc/2,ovs);
        end
        clear x_Vec
        CCx(end,:,:) = [];
        CCxq = zeros(nTimes, Nc/2,Na);
        for i=1:Na 
            CCxq(:,:,i) = pfb_real(x_Vec_q(:,i),filt_coeff,Nc/2,ovs);
        end
        clear x_Vec_q
        CCxq(end,:,:) = [];
        [Nsample,~] = size(CCx(:,:,1)); 
        toc;
        
        %% FFT equalizer   
        fprintf('Equalizing the Signals...')
        tic;
        % Compute normalization and calibration coefficients
        % norm_curve(Ncc,Na): average RMS amplitude level
        % fft_eq: bits to discard in FFT equalizer (same for all antennas)
        % cal_coef: coefficient to normalize output to (RMS_eq*2^11)
        % gain_mean: mean amplitude gain for all antennas
        % gain_corr(Ncc,Na): Gain correction to apply
        % denormalization: factor to come back to calibrated data 
        % norm_fact(Ncc,Na): Normalization/calibration factor applied
        %
        if nItt == 1
            norm_curve = squeeze(rms(CCxq,1));
            norm_mean  = mean(norm_curve,2);
            gain_tot = gain(:,1:Na) ./ RMS_x_CZ; % amplitude gain
            
            %% gain parameter is not defined from 1 to 13 channel
            %this line is necessary in order to not have for non-defined
            %channels a NaN element in the final result
            gain_tot(abs(gain_tot) == 0) = eps+1i*eps;
            gain_tot_mean = mean(abs(gain_tot),2);
            gain_mean = mean(abs(gain(:,1:Na)),2);            
            % FFT equalization, in bits to shift
            fft_eq = ceil(log2(max(norm_curve,[],2)./RMS_eq));
            fft_eq(fft_eq < 0) = 0;
            
            norm_fact =  round((RMS_eq .*2^11.*(gain_tot_mean./gain_tot.*...
                         2.^fft_eq ./norm_mean))) ./ (2^11 * 2^(eq_bit-beam_bit) );     
            % 2^11 * 2^(eq_bit-beam_bit)  =    
            %                               2^7 for 16-bit quant.
            %                               2^11 for 12-bit quant. 
            %                               2^15 for 8-bit quant.
                     
            denormalization = mean(RMS_x_CZ,2).*2^(eq_bit-beam_bit)./ ...
                             (mean(RMS_eq./(norm_mean),2).*RMS_ADC_bit);
                         
             if beam_bit == 16
                % taking into account the last 12-bit quantization  
                denormalization = 2^7 * denormalization; %
             end
             
        end
        clear gain gain_tot
        % Rescaling at FFT output
        CCx = CCx./(2.^fft_eq');
        CCxq= CCxq./(2.^fft_eq');

        if enbl_eq
            for i = 1:Na
                CCxq(:,:,i) = quantization(CCxq(:,:,i),eq_levels);
            end
        end 
        toc;

        %% Phase Delay Correction  
        fprintf('Delay and Phase correction...')
        tic; 
        f =(F0/2^(10)) * (0: 2^(9) -1).';

        %Sample-Times for the Delayed/Advanced Sequence
        SI_Vec = transpose(int64(round((T_s)*F1).' + (0 : 1 : Nsample-1)));
        ST = double(SI_Vec)/F1;
        HA = beam_HA0 + ST*HA_rate; 
        %The Time variable Delay in sec for the Time Duration
        TVD_Vec = -( [cosd(beam_DEC)*cosd(HA),...
                    -cosd(beam_DEC)*sind(HA),...
                    sind(beam_DEC)*ones(Nsample,1)]*d_Vec/c);
        
        % Only in the first iteration to plot the beamforming loss in 
        % "plot_beam_loss.m"
%         if nItt==1
%             df = (-53:54) *800e6./(128*1024);
%             dt = mean(TVD_Vec(:,:),1);
%             Phase = exp(2j*pi*dt.*df');
%             curve = abs(mean(Phase,2)).^2;
%     %         semilogy(df,curve);
%             save('beam_curve.mat','df','dt','Phase');
%             clear Phase df dt
%         end
        % Applying delays 
        for z =1:Na
            Phase_sft = exp(-2j*pi*TVD_Vec(:,z).*f'); %-
            CCx(:,:,z) = (CCx(:,:,z) .* Phase_sft);
            CCxq(:,:,z) =(CCxq(:,:,z).* Phase_sft);
        end  
        clear Phase_sft TVD_Vec
        toc;  

        %% Calibration   
        fprintf('Calibration process...')
        tic; 

        for i =1:Na
            CCx(:,:,i)  = CCx(:,:,i) .*norm_fact(:,i).'; 
            CCxq(:,:,i) = CCxq(:,:,i).*norm_fact(:,i).'; 
        end

        if enbl_beam
             for i = 1:Na
                CCxq(:,:,i) = quantization(CCxq(:,:,i),beam_levels);
             end
        end
        toc;
        
        %% Beamforming
        fprintf('Beamformed data...')
        if beam_bit ==16
            % First sum
            for j=1:(Na/8)
                CCxq_b1(:,:,j) = sum(CCxq (:, :, (1 : 8) + 8*(j-1)),3);
            end
            clear CCxq
            % for a x-12-16-12-bit quantization with RMS of ~ 0.08 - 0.22
            CCx = CCx ./ 2^7;
            CCxq_b1 = CCxq_b1 ./ 2^7;
            
            CCxq_b1 = quantization(CCxq_b1,2^12-1);

%             Second sum
            CCx_beam  = sum(CCx,3);
            clear CCx
            CCxq_beam = sum(CCxq_b1,3);
            clear CCxq_b1
        else
            CCx_beam  = (sum(CCx,3));
            CCxq_beam = (sum(CCxq,3));
            clear CCx CCxq
        end
        
    %% Correct for bandpasses and normalize 
        fprintf('Generating the composite spectrum...')
        tic;
        
        XC2_t = composite_spectrum(CCx_beam, Analysis, denormalization,...
                                 gain_mean, Polyphase.H2D);
        clear CCx_beam
        
        XCq2_t= composite_spectrum(CCxq_beam, Analysis, denormalization,...
                                 gain_mean, Polyphase.H2D);
        clear CCxq_beam
        
        % Correction for the number of antennas (Na^2) 
        % and for Poliphase filterbank factor (2^15) 
        XC2 = XC2 +   (XC2_t/ Na^2*2^30);
        XCq2 = XCq2 + (XCq2_t/Na^2*2^30);
        
        XC2_v = XC2_v + (XC2_t/Na^2*2^30).*conj(XC2_t/Na^2*2^30);
        XCq2_v = XCq2_v + (XCq2_t/Na^2*2^30).*conj(XCq2_t/Na^2*2^30);   
%   
        toc;
       
    end %end for
    
     %% Saving results and normalize
    fprintf('Saving the Results...')
    tic; 
    
    std_xc1  = sqrt((XC2_v-(XC2.*conj(XC2))/NItt)/(NItt-1)/NItt);
    std_xcq1 = sqrt((XCq2_v-(XCq2.*conj(XCq2))/NItt)/(NItt-1)/NItt);
    
    XC1      = XC2/NItt;
    XCq1     = XCq2/NItt;
    
    
    filename = ['Beam_LFAA_' num2str(Na) 'Ant_bit' num2str(samples_bit) '-'...
                num2str(eq_bit) '-' num2str(beam_bit) '_sigma_'  num2str(s1) ...
                '_T_' num2str(n_ms) 'ms.mat' ];

    save(filename,'norm_fact','norm_curve','fft_eq','nItt', ...
    'XC1', 'XCq1','std_xc1','std_xcq1', ...
    'ovs','RMS_eq','Na','F0','n_ms', ...
    'RMS_x_CZ');
    toc;

end %end function