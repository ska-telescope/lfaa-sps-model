%% IMPORTGSM_DATA - Import the Global Sky Model data
%  Import the GSM data and convert the RA and DEC of the pixels in
%  Elevation and Azimuth. Evaluate also the diffuse emission component.
% It works for the first Scenario.
%
%% Other files required: 
%                         RaDec2AzEl.m, 
%                         Skala4_1_IDS_P1.mat, 
%                         map1.txt
%                         diffuse1.txt,
%                         coord_gal1.txt
%% Author: S.Chiarucci
% INAF - OAA - Florence
% email address:  simchi@arcetri.astro.it
% June 2019; Last revision: 20/06/19

%% ------------- BEGIN CODE --------------

clear
freq=(10:10:400);% MHz
Npoint=length(freq);
Date = '2020/01/01 23:00:00';
fileID1 = fopen('map1.txt','r');
fileID2 = fopen('diffuse1.txt','r');
fileID3 = fopen('coord_gal1.txt','r');
T = fscanf(fileID1, '%f%f%f%f%f', [5 Npoint]);
D = fscanf(fileID2, '%f', [Npoint 1504]);
C = fscanf(fileID3, '%f', [1504 2]);
fclose('all');

RA =[266.4 201.7 187.7 212.1 151.3];
DEC=[-28.9 -42.9 11.8 -61.5 1.1];

[~,el,az] = RaDec2AzEl(Date,C(:,1),C(:,2),118,-26);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% l = cosd(el) .* cosd(az);
% m = cosd(el) .* sind(az);

load ('Skala4_1_IDS_P1.mat','Beam','Prad');
for j = 1 : length(freq)
   P_in(j) = Prad(j)';
   U(j,:) = Beam(j).beam(el,az);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

solidangle = 0.00409;
Diff = sum((D .* U) * solidangle,2)./P_in';

save('Scenario1.mat', 'T','Diff','RA','DEC','Date')
% loglog(freq,T','-.',freq,D)
% f=fit(log10(freq'),log10(D'),'poly1')
figure1 = figure(1);
semilogy(freq,T','-.',freq,mean(D,2),'-')
title({'Sky model for the First Scenario'});
annotation(figure1,'textbox',[0.80 0.36 0.08 0.04],'String','\lambda = -2.38',...
    'FitBoxToText','off',...
    'EdgeColor',[1 1 1]);
annotation(figure1,'textbox',[0.80 0.24 0.078 0.04],'String',{'\lambda = -2.49'},...
    'EdgeColor',[1 1 1]);
annotation(figure1,'textbox',[0.80 0.13 0.078 0.040],'String',{'\lambda = -2.50'},...
    'EdgeColor',[1 1 1]);
annotation(figure1,'textbox',[0.80 0.29 0.078 0.04],'String',{'\lambda = -2.29'},...
    'EdgeColor',[1 1 1]);
annotation(figure1,'textbox',[0.80 0.20 0.078 0.04],'String',{'\lambda = -2.50'},...
    'EdgeColor',[1 1 1]);
annotation(figure1,'textbox',[0.80 0.179 0.043 0.026],'String',{'\lambda = -2.46'},...
    'EdgeColor',[1 1 1]);
legend('#1 Source','#2 Source','#3 Source','#4 Source','#5 Source','Diffuse component');
ylabel('Temperature (K)')
xlabel('Frequency (MHz)')
grid on