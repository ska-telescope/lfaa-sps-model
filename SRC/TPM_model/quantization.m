function a = quantization(x,nlvl)
%% QUANTIZATION - Quantization of a real or complex signal
%
% Quantize the input vector to integers, with truncation at +/- nlvl
%
% Syntax:  a = quantization(x,nlvl)
%
%% INPUTS:
% 
% x = input signal vector
% nlvl = Number of quantization levels (even or odd)
%
%% OUTPUTS:
%
% a = quantized signal

%% Author: S.Chiarucci, G.Comoretto
% INAF - OAA - Florence
% email address:  simchi@arcetri.astro.it, comore@arcetri.astro.it  
% Feb 2015; Last revision: Aug-8-17
 
%% Changes
%  Author            Date           Comments
%  ---------------   -----------
%  G.Comoretto        Feb-04-15   Original version
%  S.Chiarucci        Jun-29-17   Added complex input case
%  S.Chiarucci        Aug-8-17    Added quantization with an even nlvl

%% ------------- BEGIN CODE --------------
    n = (nlvl-1)/2;   
     if isreal(x)
         if mod(nlvl,2)==1
             a = max(min(round(x),n),-n);
         else
             a = max(min(sign(x+1E-16) .* (floor(abs(x))+0.5) ,n),-n);
         end
     else
         if mod(nlvl,2)==1           
             a0 = round(x);
             a_real = max(min(real(a0),n),-n);
             a_im = max(min(imag(a0),n),-n);             
         else
             a_real = max(min(sign(real(x)+1E-16) .* (floor(abs(real(x)))+0.5) ,n),-n);
             a_im = max(min(sign(imag(x)+1E-16) .* (floor(abs(imag(x)))+0.5) ,n),-n);
         end
         a = a_real + a_im*1i;
     end
end