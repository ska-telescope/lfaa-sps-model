function [xEast, yNorth, zUp] = geo2enu(lat, lon, h, lat0, lon0, h0, Radius)
%% GEO2ENU - Geographic coordinate conversion
% The local ENU coordinates are formed from a plane tangent to the Earth's 
% surface fixed to a specific location. By convention the east axis is 
% labeled x, the north y and the up z.
%
% Syntax:  [xEast, yNorth, zUp] = geo2enu(lat, lon, h, lat0, lon0, h0, Radius)
%
%% INPUTS:
%  lat, lon, h =  latitude, longitude and altitude of the point of interest
%  lat0, lon0, h0 = latitude,longitude and altitude of the reference center
%  Radius = Earth radius
% 
%% OUTPUT:
%  xEast, yNorth, zUp = local ENU coordinte
%

%% Author: S.Chiarucci
% INAF - OAA - Florence
% email address:  simchi@arcetri.astro.it
% June 2019; Last revision: 20/06/19

%% ------------- BEGIN CODE --------------

% Cartesian offset vector from local origin to (LAT, LON, H).
s1 = sind(lat0);
c1 = cosd(lat0);
s2 = sind(lat);
c2 = cosd(lat);
p1 = c1 .* cosd(lon0);
p2 = c2 .* cosd(lon);
q1 = c1 .* sind(lon0);
q2 = c2 .* sind(lon);
u = Radius * (p2 - p1) + (h0 .* p2 - h .* p1);
v = Radius * (q2 - q1) + (h0 .* q2 - h .* q1);
w = Radius * (s2 - s1) + (h0 .* s2 - h .* s1);
cosPhi = cosd(lat0);
sinPhi = sind(lat0);
cosLambda = cosd(lon0);
sinLambda = sind(lon0);
t     =  cosLambda .* u + sinLambda .* v;
xEast = -sinLambda .* u + cosLambda .* v;
zUp    =  cosPhi .* t + sinPhi .* w;
yNorth = -sinPhi .* t + cosPhi .* w;
end