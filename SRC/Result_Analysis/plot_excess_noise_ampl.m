%% PLOT_EXCESS_NOISE_AMPL - Plot the added noise as a function of signal level
% Plot the excess noise as a function of signal level for a range of
% frequency
%
% Syntax:  plot_excess_noise_ampl
%
%% Inputs:
% None : 
% need to load *.mat files 
%
%% Outputs:
% None : 
% plot the excess noise as a function of signal level
%
%%  Mat files:
%           - *.mat files from "tpm_model _beamforming.m",
%             e.g. "LFAA_2Ant_bit8-0-0_sigma_0.12_RFI_0_T_200ms.mat";

%% Author: S.Chiarucci
% INAF - OAA - Florence
% email address:  simchi@arcetri.astro.it
% June 2018; Last revision: 20/06/18

%% ------------- BEGIN CODE --------------
% close all
clear

len=55242;
F0=800e6;
% Select frequency regions
a=[7970    1.414e4 2.094e4 2.448e4 3.04e4 4.1e4];
b=[1.281e4 1.655e4 2.351e4 2.964e4 3.303e4 4.737e4];

f=F0*(a+b)/2/(len-1)/2;

%     n=0:len-1;
%     N=n*F0/(len-1)/2;
        
    filename = {'LFAA_2Ant_bit8-0-0_sigma_11ADU_RFI_0_T_200ms.mat',...
            'LFAA_2Ant_bit8-0-0_sigma_14ADU_RFI_0_T_200ms.mat',...
            'LFAA_2Ant_bit8-0-0_sigma_17,5ADU_RFI_0_T_200ms.mat',...
            'LFAA_2Ant_bit8-0-0_sigma_20ADU_RFI_0_T_200ms.mat',...
            'LFAA_2Ant_bit8-0-0_sigma_23ADU_RFI_0_T_200ms.mat',...
            'LFAA_2Ant_bit8-0-0_sigma_26ADU_RFI_0_T_200ms.mat',...
            'LFAA_2Ant_bit8-0-0_sigma_29ADU_RFI_0_T_200ms.mat',...
            'LFAA_2Ant_bit8-0-0_sigma_32ADU_RFI_0_T_200ms.mat',...
            'LFAA_2Ant_bit8-0-0_sigma_35ADU_RFI_0_T_200ms.mat'};
        
 filename = {'LFAA_2Ant_bit8-12-8_sigma_11ADU_RFI_0_T_200ms.mat',...
    'LFAA_2Ant_bit8-12-8_sigma_14ADU_RFI_0_T_200ms.mat',...
    'LFAA_2Ant_bit8-12-8_sigma_17,5ADU_RFI_0_T_200ms.mat',...
    'LFAA_2Ant_bit8-12-8_sigma_20ADU_RFI_0_T_200ms.mat',...
    'LFAA_2Ant_bit8-12-8_sigma_23ADU_RFI_0_T_200ms.mat',...
    'LFAA_2Ant_bit8-12-8_sigma_26ADU_RFI_0_T_200ms.mat',...
    'LFAA_2Ant_bit8-12-8_sigma_29ADU_RFI_0_T_200ms.mat',...
    'LFAA_2Ant_bit8-12-8_sigma_32ADU_RFI_0_T_200ms.mat',...
    'LFAA_2Ant_bit8-12-8_sigma_35ADU_RFI_0_T_200ms.mat'};   
% the sig vector must fit the input signal level in the mat files
ampl=[11 14 17.5 20 23 26 29 32 35]';
ratio =zeros(length(f),length(filename));
ratio2=zeros(length(f),length(filename));
for i=1:length(f)
    for j = 1:length(filename)

        load (char(filename(j)));
        qratio = var(XCq1(a(i):b(i),2) - XC1(a(i):b(i),2) );
        var_norm = var(XC1(a(i):b(i),2));
%         ratio(i,j) = qratio./var_norm/2?
%        ratio(i,j) = std(XCq1(a(i):b(i),2)-XC1(a(i):b(i),2)).^2 ./ std(XC1(a(i):b(i),2)).^2;

    end
end    
figure
semilogy(ampl, ratio,'Marker','h');
hold on

% figure(3)
% semilogy(ampl, 1-ratio2 ,'Marker','h');
% hold on

    
legend('\nu_m = 75 MHz','\nu_m = 114 MHz','\nu_m = 157 MHz','\nu_m = 196 MHz','\nu_m = 231 MHz','\nu_m = 312 MHz')
 xlabel('Signal RMS (Re peak digital level)');
% Create ylabel
ylabel('Excess noise');
grid on