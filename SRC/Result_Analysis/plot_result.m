%In order to plot the result you need the dataplot.mat and the .mat output
%file
Na=256; %n.of antennas used in the simulation
len=length(XCq1);
freq = (0:32768-1)'*800e6/(32768)/2;
ff=(0:511)'*800e6/512/2;
n=0:len-1;
N=n*F0/(len)/2; % 
% semilogy(N/1e6,smooth(abs(XCq1),10))
semilogy(N/1e6,abs(XCq1))
hold on

semilogy(freq/1e6,Sky/4/pi*((1.9/180*pi)^2*pi)+(Noise./Na)./calbeam,'--');
semilogy(freq/1e6,Sky/4/pi*(1.9/180*pi)^2*pi);