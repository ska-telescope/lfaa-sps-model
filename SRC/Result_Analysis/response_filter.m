function a = response_filter(f,h,Nc,Os)
%% RESPONSE_FILTER- First filter response
% Computed using a spline approximation of the actual filter response
%
% Syntax:  a = response_filter(f,h,Nc,Os)
%
%% INPUTS: 
% f: frequency, normalized to Nyquist (oversampled). vector or matrix
% h: Filter taps
% Nc: Number of channels (decimation)
% Os: Oversampling factor
%
%% OUTPUTS:
% a: amplitude response
%
% See also:  COMPOSITE_SPECTRUM
%
%% Other m-files required:
%  fft_ir.m

%% Author: G.Comoretto
% INAF - OAA - Florence
% email address: comore@arcetri.astro.it  
% June 2018; Last revision: 20/06/18

%% ------------- BEGIN CODE --------------
  len =2^20;
  spt =abs(fft_ir(h,len/2));
  scale = 0.5*len*Os/Nc; % Point at Nyquist frequency
  Ns = round(scale);
  Fs = (0:Ns)/scale;
  pp = spline(Fs, spt(1:(Ns+1)));
  clear h spt;
  f = abs(f); % symmetric response
  a = ppval(pp, f);
end