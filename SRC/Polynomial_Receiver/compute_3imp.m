function [freq_ip3,amp_ip3]= compute_3imp(rfi_in)
%% compute_3imp - Compute the 3rd intermodulation products of a list of lines
%
% Input and output in dBm for nominally IP3=0dB
% Output is sorted in order of increasing frequency, 
% with duplicates summed together
%
% Syntax:  [freq_ip3,amp_ip3]= compute_3imp(rfi_in)
%
%% Input
% rfi_in:  nx3 table of lines. 
%	Column 1 is frequency,
%	Column 2 is input amplitude (dBm),
%	Column 3 is gain (dB)
%
%% Output: 
% freq_ip3: list of frequencies for the 3rd order intermodulation products
% amp_ip3:  list of amplitudes at output (dBm)
% 
%% Author: S.Chiarucci
% INAF - OAA - Florence
% email address: simchi@arcetri.astro.it  
% Jan 2019 - Last revision: 05-03-2019

%% note
% the equation IMD3 = 3Prfi - 2IP3 
% replaces only the relation IMD = 20log(3/4 * k3 * Ai^2 Aj) 
% where k3 = 4/3 * 1/IP3 , A = Amax/sqrt(2) and Aj may be = Ai; 

%% ------------- BEGIN CODE --------------

   k = size(rfi_in,1);
   freq=zeros(k,k,k);
   amp_i=zeros(k,k,k);
   ind=0;
   addfreq = zeros(nchoosek(length(rfi_in(:,1)),3),2);
   for i = 1:k
     for j = i:k
        for z = j:k
            if i == j
                if (j == z)  
                    freq(i,i,i)  = 3*rfi_in(i,1);
                    amp_i(i,i,i) = 3*(rfi_in(i,2)) - 20*log10(3);
%                     the coefficeint 20*log10(3) comes from : 1/4 * 4/3
                else
                    freq(i,i,z) = 2*rfi_in(i,1) + rfi_in(z,1);
                    freq(z,z,i) = 2*rfi_in(z,1) + rfi_in(i,1);
                    
                    amp_i(i,i,z) = 2*rfi_in(i,2) + rfi_in(z,2);                           
                    amp_i(z,z,i) = 2*rfi_in(z,2) + rfi_in(i,2);                        
                end  
            elseif  (i ~= z) && (j ~= z)
                freq(i,j,z) = rfi_in(i,1) + rfi_in(j,1) - rfi_in(z,1);
                freq(i,z,j) = rfi_in(i,1) + rfi_in(z,1) - rfi_in(j,1);
                freq(j,z,i) = rfi_in(j,1) + rfi_in(z,1) - rfi_in(i,1);
                ind = ind+1;
                addfreq(ind,1) = rfi_in(i,1) + rfi_in(j,1) + rfi_in(z,1);
                
                amp_i(i,j,z) = rfi_in(i,2) + rfi_in(j,2) + rfi_in(z,2) + ...
                              + 20*log10(2);
%                     the coefficeint 20*log10(2) comes from : 3/2 * 4/3                         
                amp_i(i,z,j) = amp_i(i,j,z);
                amp_i(j,z,i) = amp_i(i,j,z);
                addfreq(ind,2) = amp_i(i,j,z);
                
            else %(i or j are == z)
                freq(i,j,i) = 2*rfi_in(i,1) - rfi_in(j,1); 
                freq(i,j,j) = 2*rfi_in(j,1) - rfi_in(i,1);
                
                amp_i(i,j,i) = 2*(rfi_in(i,2)) +(rfi_in(j,2));
                amp_i(i,j,j) = 2*(rfi_in(j,2)) +(rfi_in(i,2));        

            end
        end
     end
   end
   
%% Sort lines in order of increasing frequency
   freq = reshape(abs(freq),[],1);
   freq(freq==0) =[];
   freq = [freq ; addfreq(:,1)];
   amp1 = reshape(amp_i,[],1);
   amp1(amp1==0) = [];
   amp = [amp1; addfreq(:,2)];
   [~,i]=sort(freq);
   
   freq_ip3=freq(i);
   amp_ip3 = amp(i,:);
   
   filename = 'Pol_receiver_IMD.mat';
   save(filename,'freq_ip3','amp_ip3','-append');
