function signal = tpm_LFAA_spect(Phase_sft,Delay_Specs)
%% TPM_LFAA_SPECT - Signal samples builder
% Generates a frame of a vector of samples with predefined spectrum,
% correlation function, and phase shift (delay)
%
% Calcolated for Ns samples, Na antennas, (Ns,Na) derived from Delay_specs sizes
% 
% On first call computes needed quantities in a persistent array, 
% Subsequent calls DO NOT specify Delay_Specs: 
%  signal = tpm_LFAA_spect(Phase_sft)
% 
% Computes a pseudorandom Gaussian noise, shapes it in the frequency domain
% both for the correlated and uncorrelated portions, shifts it in the 
% frequency domain for the given delay, and transforms back to the time domain.
% Each segment is multiplied by a cos(t) tapering, and summed with the 
% second half of the previous segment. The first half is returned, and
% the second s stored in the persistent array to be used in the next call
%
% Syntax:  signal = tpm_LFAA_spect(Phase_sft,Delay_Specs)
%
%% INPUTS:
%  Phase_sft	- Phasor for delay: size(2*Ns, Na)
%  Delay_specs  - Structure with delay specifications. Elements:
%    .Filter_noise      Amplitude for noise+RFI (uncorrelated) component
%    .Filter_sky        Amplitude (complex) for sky (correlated) component
%
% Delay_specs are specified only in the first call, and stored in 
%   a persistent array 
% Filter_noise and Filter_sky specify the amplitude response in the frequency
%   domain, from frequency 0 to 0.5, and include the receiver response. 
% Filter_noise (size (Ns,1)) includes an averaged amplitude response
% Filter_sky (size (Ns, Na)) includes a complex response, one per antenna
%
%% OUTPUTS:
%  signal - vector of (Ns, Na) samples, one column per antenna
%
% RMS amplitude of signal is 
%    sqrt(mean(abs(Filter_noise).^2 + abs(Filter_sky).^2))
%
%
% See also: TPM_LFAA_SIGNAL
%
%% Author: S.Chiarucci, G.Comoretto
% INAF - OAA - Florence
% email address:  simchi@arcetri.astro.it, comore@arcetri.astro.it  
% June 2018; Last revision: 20/06/18

%% ------------- BEGIN CODE --------------

    persistent n n2 ncol filt_n filt_s taper nsource  o_e;
    persistent old_a Narray s;
    if  nargin == 2
        o_e = 1;  % parity
        Narray = [Delay_Specs.N_array Delay_Specs.N_array]';
        layout = Delay_Specs.layout;
        filt_uncorr = Delay_Specs.Filter_noise;
        filt_corr = Delay_Specs.Filter_sky;
        [n,ncol,nsource] = size(filt_corr);
        taper = cos(((0:(2*n-1))'-n)*pi/(2*n));
        n2  = size(taper,1);
%         n1 = flipud(conj(filt_uncorr(2:end)));
%         filt_n = [filt_uncorr; 0.; n1];
%         filt_n(1) = abs(filt_n(1));
%         filt_n(n+1) = abs(filt_n(n));
        %%
        a = zeros(n2,ncol);
        a1 = zeros(n2,nsource);
        s = cell(2,nsource+1);
        for i=1:nsource
           
            if Narray(o_e,i) <= 0 
                %Set the pseudorandom generators for the i-th source ("real" signal)
                rng(10+i+700);%It ranges from 711 to 715
                [~] = randn(abs(Narray(o_e,i)),1);
                s{2,i} = rng;
                rng(i+700); %It ranges from 701  to 705
                [~] = randn(abs(Narray(o_e,i)),1);
                Narray(:,i) = 0;
                a1(:,i) = fft(randn(n2,1));
                s{1,i} = rng;
                continue
            end

            if Narray(o_e,i) > 0
                %initialize pseudorandom generators
                % s{2,i} = rng(610+i+10); !!!Not working command!!!
                rng(610+i);%It ranges from 611 to 615
                s{2,i} = rng;
                rng(600+i);%It ranges from 601 to 605 
                s{1,i} = rng;
             %Load the pseudorandom generator for the i-th source (positive delay)   
                rng(s{o_e,i}); 
                if Narray(o_e,i) > n2
                        a1(:,i) = fft(randn(n2,1));
                        s{o_e,i} = rng;
                        Narray(o_e,i) = Narray(o_e,i)-n2;
                       continue
                end
                temp = randn(Narray(o_e,i),1);
                Narray(o_e,i) = 0;
                rng(i+700);
                a1(:,i) = fft( vertcat(temp,randn(n2-length(temp),1)) );
                s{1,i} = rng;
            end
        end
        
 %Set the pseudornadom generator for the uncorrelated signal
 %It ranges from 0 (Array center) to 512
        rng(layout);
        for i=1:ncol
            n1 = flipud(conj(filt_uncorr(2:end,i)));
            filt_n(:,i) = [filt_uncorr(:,i); 0.; n1];
            filt_n(1,i) = abs(filt_n(1,i));
            filt_n(n+1,i) = abs(filt_n(n,i));
            for j=1:nsource
                corr_source=squeeze(filt_corr(:,:,j));
                s1 = flipud(conj(corr_source(2:end,i)));
                filt_s(:,i,j) = [corr_source(:,i);0.; s1];
                filt_s(1,i,j) = abs(filt_s(1,i,j));
            end
            a_n = fft(randn(n2,1)).*filt_n(:,i);
            a(:,i) = real( ifft( sum(a1.* squeeze(filt_s(:,i,:)) .* ...
                            squeeze(Phase_sft(:,i,:)),2) + a_n) );            
        end
        old_a = a((n+1):(end),:).*taper((n+1):(end),:);
        clear a
        s{1,nsource+1} = rng;
    end
    
    %%
    o_e = abs(o_e-2)+1; %switch between 1 and 2
    a = zeros(n2,ncol);
    for i = 1:nsource  
        if Narray(o_e,i) > 0
            rng(s{o_e,i}); %Load the pseudorandom generator
            if Narray(o_e,i) > n2
                a1(:,i) = fft(randn(n2,1));
                s{o_e,i} = rng;
                Narray(o_e,i) = Narray(o_e,i)-n2;
                continue
            end
            temp = randn(Narray(o_e,i),1);
            Narray(o_e,i) =0;
            rng(700+i+10*(o_e-1)) 
            a1(:,i) = fft(vertcat(temp,randn(n2-length(temp),1)));
            s{o_e,i} = rng;
            continue
        end
        rng(s{o_e,i});
        a1(:,i) = fft(randn(n2,1));
        s{o_e,i} = rng;
    end
    rng(s{1,end});
    for i=1:ncol
        a_n = fft(randn(n2,1)).*filt_n(:,i);
        a(:,i) = real( ifft( sum(a1.* squeeze(filt_s(:,i,:)) .* ...
                 squeeze(Phase_sft(:,i,:)),2) + a_n) ).*taper; 
    end
    s{1,nsource+1} = rng;
    signal = old_a + a(1:n,:);
    old_a = a((n+1):(end),:); 
        
end