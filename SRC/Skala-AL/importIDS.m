%% importIDS - import data from IDS simulation
%  
% From the IDS file defined in 'filename' variable, the script saves in 
% a mat file the electric field component and the directivity for a given 
% polarization. The saved data have channel x phi x theta dimension.
%
%% Author: S.Chiarucci
% INAF - OAA - Florence
% email address:  simchi@arcetri.astro.it
% April 2019;

%% ------------- BEGIN CODE --------------
clear
freq=(10:10:400);% MHz
% freq=(20:10:350);
theta=(0:1:90)'; % angular interval used by IDS
elevation = 90-theta;
phi=(0:5:355)';% angular interval used by IDS
azimuth = mod(360- phi+90,360);
Z=376.73;%Impedance of free space
c=physconst('lightspeed'); % metri/secondi
[xq,yq] = meshgrid(elevation,azimuth);
% for i = 1: length(phi)
%     azimuth = 360- phi(i,1)+90;
% %     azimuth90 = phi90(i,1);
%      %conversion to l,m coordinates
%      l(i,:) = sind(elevation) .* cosd(azimuth);
%      m(i,:) = sind(elevation) .* sind(azimuth);
% end
Ntheta=length(theta);
Nphi=length(phi);
Npoint=Ntheta*Nphi;
F = struct;
Beam = struct;
Directivity = struct;
Ludwig = struct;
zCF=[0, 0, 0, 0, 0, 0.1, 0.15, 0.2, 0.39, 0.6, 1, 1.35, 1.4, 1.35, 1.25, 1.3,1.4, 1.6, 1.75, 1.8,...
     1.75, 1.75, 1.75, 1.75, 1.8, 1.81, 1.81, 1.9, 1.9,1.9, 1.9, 1.9, 1.9 1.9, 1.95, 1.95, 1.95, 1.95, 1.95, 1.95];
% zCF=[0, 0, 0, 0, 0.1, 0.15, 0.2, 0.39, 0.6, 1, 1.35, 1.4, 1.35, 1.25, 1.3,1.4, 1.6, 1.75, 1.8,...
%      1.75, 1.75, 1.75, 1.75, 1.8, 1.81, 1.81, 1.9, 1.9,1.9, 1.9, 1.9, 1.9 1.9, 1.95]; 
Nfreq=size(freq,2);

%%
phGeo = zeros(Nfreq,Ntheta); % "Theta" e "Phi" sono angoli misurati rispetto al centro dell'array
for j=1:Ntheta           
    phGeo(:,j)=(2*pi*freq'.*1e6/c).*(zCF'.*cosd(theta(j)));
end
phGeo = repmat(phGeo,[1,Nphi]);

%%
for j =1:length(freq)
    filename = ['Lasco_fili_10-350MHz_P1_for_output_' num2str(freq(j)) '.ffd'];
%     filename = ['Lasco_fili_10-350MHz_P2_for_output_' num2str(freq(j)) '.ffd'];
    savename= 'Skala4_1_IDS_P1.mat';
    
    fileID = fopen(filename);
    formatspec = '%f%f%f%f%f%f%f%f%f';

    dataIDS = textscan(fileID, formatspec,Npoint,'CommentStyle',{'#'});
    F(j).chan   = freq(j);
    F(j).data   =  cell2mat(dataIDS);

    Etheta = F(j).data(:,8) + 1i.* F(j).data(:,9); % electric field theta component
    Ephi   = F(j).data(:,6) + 1i.* F(j).data(:,7); %electric field phi component
    P_in   = F(j).data(:,4);
    Ephi_phase  = exp(-1i.* (angle(Ephi) - phGeo(j,:)'));
    Etheta_phase= exp(-1i.*(angle(Etheta)- phGeo(j,:)'));
    
    F(j).Etheta = reshape(Etheta,Ntheta,Nphi).';
    F(j).Ephi   = reshape(Ephi,Ntheta,Nphi).';
    F(j).P_in   = reshape(P_in,Ntheta,Nphi).';
    F(j).Etheta_ph = reshape(Etheta_phase,Ntheta,Nphi).';
    F(j).Ephi_ph =   reshape(Ephi_phase,Ntheta,Nphi).';
end

Etheta = permute(cat(3,F.Etheta),[3,1,2]); % channel x phi x theta
Ephi   = permute(cat(3,F.Ephi),[3,1,2]); 
Ephi_phase  = permute(cat(3,F.Etheta_ph),[3,1,2]);
Etheta_phase= permute(cat(3,F.Ephi_ph),[3,1,2]);
P_in   = permute(cat(3,F.P_in),[3,1,2]); 

for j=1:length(freq)
    Ep = squeeze(Ephi(j,:,:));  %phi component
    Et = squeeze(Etheta(j,:,:));%theta component
    Epph = squeeze(Ephi_phase(j,:,:));  %phi component
    Etph = squeeze(Etheta_phase(j,:,:));%theta component
    Pi = squeeze(P_in(j,:,:));
    U = (abs(Et).^2 + abs(Ep).^2)/2/Z; % radiation pattern
    Dir = 4*pi*U./Pi;
    % Valuto la co-pol e cross-pol secondo la 3def-Ludwig        
    % componente R 
    Co = (abs(Et).*Etph) .* sind(phi) + (abs(Ep).*Epph).*cosd(phi);
    % componente C 
    Cr = (abs(Et).*Etph) .* cosd(phi)- (abs(Ep).*Epph).*sind(phi);
    Gain_Co = 4*pi*abs(Co).^2 ./Pi/2/Z;
    Gain_Cr = 4*pi*abs(Cr).^2 ./Pi/2/Z;
    
    Directivity(j).beam = scatteredInterpolant(xq(:),yq(:),Dir(:));
    Beam(j).beam = scatteredInterpolant(xq(:),yq(:),U(:));
%     Ludwig(j).Cr = scatteredInterpolant(l(:),m(:),Cr(:));
%     Ludwig(j).Co = scatteredInterpolant(l(:),m(:),Co(:));
    Ludwig(j).GainP1_Co = scatteredInterpolant(xq(:),yq(:),Gain_Co(:));
    Ludwig(j).GainP1_Cr = scatteredInterpolant(xq(:),yq(:),Gain_Cr(:));
end

Prad = squeeze(P_in(:,1,1));
save(savename, 'Beam','Directivity','Prad','Ludwig');