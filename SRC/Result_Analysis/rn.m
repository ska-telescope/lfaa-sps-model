function r=rn(l1, l2, rho, n)
%% RN - Computes the n-level correlation product for a bivariate distribution 
% Computes the n-level correlation product for a bivariate distribution 
% with correlation rho, quantized with n levels 
% with a quantization step of l1, l2 times the signal stdev. 
%
% Syntax:    r=rn(l1, l2, rho, n)
%
%% Inputs:
% l1, l2 = quantization step in units of the distribution sigma
% rho    = correlation coefficiet of the distribution
% n      = Number of quantization levels
%
%% Constrains
%
% -1<rho<1
% n MUST BE ODD
% for n odd use rn(l1,l2, rho,n)
%
%% Outputs:
% r= correlation product
%
% Other files required: l.mexa64 and l.cc
%
% See also: RNP.m

%% Author: G.Comoretto
% INAF - OAA - Florence
% email address: comore@arcetri.astro.it  
% 20-Dic-14 

%% ------------- BEGIN CODE --------------

   n1=(n-1)/2;
   sum=-(n1*n1);
   sqrt2=sqrt(0.5);
   for i = 1:n1
       li=l1*(i-0.5);
       sum = sum + n1*erf(li*sqrt2);
       for j=-(n1-1):n1
           sum = sum +2*l(li, l2*(j-0.5), rho);
       end
   end
   r=sum;
   return
end