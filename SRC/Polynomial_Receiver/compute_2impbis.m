function [freq_ip2,amp_ip2]= compute_2impbis(rfi_in)
%% compute_2imp - Compute the 2nd intermodulation products of a list of lines
%
% Input and output in dB, for nominally IP2=0dB
% Output is sorted in order of increasing frequency, 
% with duplicates summed together
%
% Syntax:  [freq_ip2,amp_ip2]= compute_2imp(rfi_in)
%
%% Input
% rfi_in:  nx3 table of lines. 
%	Column 1 is frequency,
%	Column 2 is input amplitude (dB),
%	Column 3 is gain (dB)
%
%% Output: 
% freq: list of frequencies for intermodulation products
% amp:  list of amplitudes at input (col. 1) and output (2) (dB)
% 
%% Author: G.Comoretto
% INAF - OAA - Florence
% email address: comore@arcetri.astro.it  
% Dec 2018; Last revision: 05-03-2019
%
%% Changes
% S. Chiarucci March 2019

%% ------------- BEGIN CODE --------------

   k = size(rfi_in,1);
   freq=zeros(k);
   amp_i = zeros(k);
   IP2 = 4.683;
   c2 = 1/sqrt(10^((IP2)/10))/2;
   amp_rfi_in  = sqrt(2) * 10.^(rfi_in(:,2)/20);  
   
   for i = 1:k
     for j = 1:k
       if i < j
         freq(i,j) = abs(rfi_in(i,1) - rfi_in(j,1));
%          fprintf('f%i - f%i\n', i,j)
       else
         freq(i,j) = rfi_in(i,1) + rfi_in(j,1);
%          fprintf('f%i + f%i\n', i,j)
       end
       amp_i(i,j) = c2 * amp_rfi_in(i) * amp_rfi_in(j);
     end
     amp_i(i,i) = amp_i(i,i)/2; 
   end

   % Sort lines in order of increasing frequency
   freq = reshape(freq,[],1);
   amp =  [reshape(amp_i,[],1)];
   [~,i] = sort(freq);
   f=freq(i);
   a = amp(i,:).^2;
 
   %% Elminate duplicates, summing together power for lines with same frequency
   j=1;
   k = max(f);
   for i = 1:k
    ind = find(f(:) == i);
    if  isempty(ind)
     continue
    end
    freq_ip2(j,1) = i;
    a1(j,1) = sum(a(ind(1):ind(end),1));
    j=j+1;
   end
   
    % Copy back results
    amp_ip2 = 10*log10(a1);

% plot(freq_ip2,amp_ip2+40.815,'+')
% grid on;hold on;
% load('RFI_HD_IMD_AAVS1.mat', 'rfi_out')
% plot(rfi_out(:,1),rfi_out(:,2),'x')

   filename = 'Pol_receiver_IMD.mat';
   save(filename,'freq_ip2','amp_ip2','-append');
end