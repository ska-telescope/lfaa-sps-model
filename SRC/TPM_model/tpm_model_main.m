function tpm_model_main(samples_bit,eq_bit,beam_bit,s1,n_ms,Na,IMDGain)
%% TPM_MODEL_MAIN - Tile Processing Module simulation 
%
% This MATLAB function performs a 'Running Simulation' of the Tile Processing 
% Module of the LFAA. This version takes into account only a small number 
% of antenna since its aim is to analyze the quantization and channelizing
% effects.  
%
% The test vector is composed of 2 or 3 independently placed antennas,
% pointing in a fixed direction in the sky. The sky (or in-beam) spectrum
% is specified in the Generator structure inside Parameters.mat. 
%
% Each antenna has a complex response given in Antenna structure. 
% The receiver noise + incoherent RFI spectrum is given in the Generator 
% structure and it is common to all antennas. 
%
% A set of monochromatic lines directed towards a fixed position may be
% added to the whole signal. Line frequency and intensity is specified as a 
% function call parameter. 
% All quantizations are optional and specified as function parameters.
% The output is stored in a mat file.
% 
% Processing is performed as: 
% - Initial quantization
% - Polyphase filterbank
% - Equalization and requantization
% - Phase Delay Correction  
% - Calibration 
% - Final quantization
%
% After the last quantization signals are correlated and saved for analyzing
% purposes. 
%
% Syntax:
% tpm_model_main(samples_bit,eq_bit,beam_bit,s1,n_ms,Na,IMDGain)
%
%% INPUTS:
%   samples_bit = n. of bits used for the ADC quantization;
%   eq_bit = n. of bits used in the quantization after the equalization
%   stage ;
%   beam_bit = n. of bits used in the final quantization;
%   s1 = Signal amplitude normalized to the clipping level,
%        0 < s1 < 1;
%   n_ms = Integration time (ms);
%   Na = Number of antennas used in the simulation;
%   IMDGain = 1000s IMD sensibility factor
%
%% OUTPUTS:
%   XC1 = unquantized auto-(1st column) and cross-(2nd column) correlation 
%   as a function of frequency channels;
%   XCq1 = same as above but with quantization;
%   std_xc1, std_xcq1 = standard deviation for XC1 and XCq1;
%   XC2 = unquantized auto-(1st column) and cross-(2nd column) correlation 
%   as a function of frequency channels for the beamformed signal;
%   XCq2 = same as above but with quantization;
%   std_xc2, std_xcq2 = standard deviation for XC2 and XCq2;
%   AC,ACq = Auto correlation without (with) quantization for the original
%   data, i.e. before channelizing process;
%
%% Example 
%    If the function 
%    "tpm_model_main(8, 12, 8, 0.2, 0, 200e6, 200, 16)"
%    is launched, we will get the output mat file
%    "LFAA_16Ant_bit8-12-8_sigma_0.2_RFI_0_T_200ms.mat"
%
%% Files required
% Other m-files required: tpm_LFAA_signal.m, tpm_RFI.m, quantization.m,
%                      cross_spect.m, pfb_real.m, composite_spectrum.m
%
% Most parameters are in fixed configuration files:
% MAT-files required: Parameters.mat, Analysis_full.mat, Analysis_128.matTILE-07_ANT-133_POL-Y_2019-05-03_041032_853531
%
% See also: TPM_STARTER,  TPM_MODEL_PULSAR, TPM_MODEL_BEAMFORMING

%% Author: S.Chiarucci, G.Comoretto
% INAF - OAA - Florence
% email address:  simchi@arcetri.astro.it, comore@arcetri.astro.it  
% June 2018; Last revision: 20/02/20

% ------------- BEGIN CODE --------------
    %% pseudorandom generator initialization 
    rng('default');
    %% Simulation Specs  
    fprintf('Configuring parameters and signals...')
    load('Parameters.mat',...
        'Antenna','Beamformer','Calibration','Generator','Polyphase','RFI');
    load('Analysis_128.mat', 'Analysis');
    position = Antenna.pos(1:Na,:);
    % load latitude and longitude of the station and array center
    lat = Antenna.StationLatitude;
    lon = Antenna.StationLongitude;
    layout = Antenna.Stationlayout; 
    lat0 = Antenna.Arraylat;
    lon0 = Antenna.Arraylon;
%     lat = lat0;
%     lon = lon0;
%     layout = 0;
%%
    %From Geodetic to ENU
    Radius = 6371000;
    [xEast,yNorth,zUp] = geo2enu(lat,lon,0,lat0,lon0,0,Radius);
    % Antenna Positions | The Distance vector in Cartesian Coordinates with 
    % respect to the Array Reference Point
    %the antenna coordinate are alredy in ENU ref
    Ant_pos = [position(1:Na,1), position(1:Na,2), zeros(Na,1)]; 
    
    % From ENU to ECEF
    d_Vec_Array = [-sind(lon0) -sind(lat0)*cosd(lon0) cosd(lat0)*cosd(lon0); ...
                   cosd(lon0)  -sind(lon0)*sind(lat0) cosd(lat0)*sind(lon0); ...
                               0           cosd(lat0)            sind(lat0)] ...
                  * [xEast yNorth zUp].';
         
    d_Vec = [-sind(lon) -sind(lat)*cosd(lon) cosd(lat)*cosd(lon); ...
              cosd(lon) -sind(lon)*sind(lat) cosd(lat)*sind(lon); ...
                      0           cosd(lat)            sind(lat)] *Ant_pos.';
    
    Nsources = size(Generator.Filter_sky,3);
    Generator.Filter_sky = Generator.Filter_sky(:,1:Na,1:Nsources);              
    Generator.Filter_noise = Generator.Filter_noise(:,1:Na);
    HA_rate = Antenna.HA_rate;
    HA0 = Antenna.Source_HA0(1:Nsources);
    Ns = Generator.filter_size;
    c = physconst('LightSpeed');
    F0 = Generator.Sample_freq;
    RFI_Az = RFI.azimuth;
    RFI_Elev = RFI.elevation;
    RFI_amp = RFI.rfi_amp;
    RFI_freq = RFI.rfi_freq;
    ovs = Polyphase.Num / Polyphase.Den;
    filt_coeff = Polyphase.H2D;
    Nseg = length(filt_coeff);
    Nc = Polyphase.Nc;
    F1 = F0 / Nc * ovs;
    beam_DEC = Beamformer.Source_DEC;
    beam_HA0 = Beamformer.HA0;
    gain = Calibration.gain(:,1:Na);
    
    %%
%     load('polyfilt_16k.mat','h','nchan');
%     Generator.Filter_sky = zeros(Ns,2,5);
%     Generator.Filter_noise =zeros(Ns,1);       
%%
    %Number of Samples per Input Sample-Block
    Ns_O = Ns*27*16;   
    %The Duration of a single frame
    TD  = Ns_O/F0; % In sec
    %The Duration Needed to Fill the Pipe-Lines 
    TOV = Nseg/F0;
    %The duration of 32k-samples
    T0 = Ns/F0;
    %The duration of the simulation
    T_s = n_ms *1E-3; %in sec
    %Number of iterations required to cover the Simulation Duration
    NItt = ceil(T_s/(TD+TOV));
    
    %Load the receiver polynomial parameters
    Oip2=49;
    Oip3=31;
    ip2_ADU2 = 10^(Oip2/10)* (17.5^2/10^(-2.7/10));
    ip3_ADU2 = 10^(Oip3/10)* (17.5^2/10^(-2.7/10));
    a2 = 1 / sqrt(ip2_ADU2)/sqrt(2);
    a3 = 4/3/ip3_ADU2/2;
    
    %% Quantization Specifications
    % RFI stage
    if RFI_amp==0
        enbl_RFI = false;
    else
        enbl_RFI=true;
    end
    % ADC quantization
    if samples_bit==0
        enbl_samples = false; % set false(true) to disable (enable) the quantizer
        RMS_ADC_bit = ((8-1)/2) * s1;
    else
        enbl_samples = true;
        samples_levels = 2^samples_bit-1; 
        RMS_ADC_bit = ((samples_levels-1)/2) * s1;
    end 
    % Equalizer quantization
    if eq_bit==0
        enbl_eq = false; % set false(true) to disable (enable) the quantizer
        RMS_eq = 2047 * 0.20*sqrt(2);
    else
        enbl_eq = true;
        eq_levels = 2^eq_bit-1; 
        RMS_eq = (eq_levels-1)/2 * 0.20*sqrt(2);
    end
    % final quantization
    if beam_bit==0
        enbl_beam = false; % set false(true) to disable (enable) the quantizer
    else
        enbl_beam = true;
        beam_levels = 2^beam_bit-1; 
    end
    
     %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%   
    Delay_Specs.d_Vec = d_Vec;
    Delay_Specs.d_Vec_Array = d_Vec_Array;
    Delay_Specs.HA_rate = HA_rate;
    Delay_Specs.F0 = F0;
    Delay_Specs.Filter_noise = Generator.Filter_noise;
    Delay_Specs.c = c;
    Delay_Specs.Ns = Generator.filter_size;
    Delay_Specs.Na = Na;
    Delay_Specs.layout = layout;

    Delay_Specs.HA0 = HA0;
    Delay_Specs.Source_DEC = Antenna.Source_DEC(1:Nsources); 
    Delay_Specs.Source_RA = Antenna.Source_RA(1:Nsources);
    Delay_Specs.Filter_sky = Generator.Filter_sky; 
    %Generate the first segment     
    first_segment= tpm_LFAA_signal(Delay_Specs,Ns,0,Nsources);   
    first_segment(1:end-Nseg,:) = [];

    nTimes = floor((Ns_O)/ (Nc/2/ovs)/2)+1;
%     N_tot = Nc/2*Analysis.n_ovs-Analysis.n_ovs/2;
    N_tot = Nc/2*Analysis.n_ovs;
    XC1     = zeros(N_tot,1);
    XCq1    = zeros(N_tot,1);
    XC1_v   = zeros(N_tot,1);
    XCq1_v  = zeros(N_tot,1);    
    XC2     = zeros(N_tot,1);
    XCq2    = zeros(N_tot,1);
    XC2_v   = zeros(N_tot,1);
    XCq2_v  = zeros(N_tot,1);
    RMS_x_CZ = 1;

    for nItt = 1:NItt

        fprintf('Running the Iteration %d of %d\n',nItt,NItt);
        T_s = (nItt-1)*TD + (T0-TOV);  %In sec
       
        %% Generating Signal
        fprintf('Generating Random Signal Sequence...')
        tic;
        signal = tpm_LFAA_signal(Delay_Specs,Ns_O,T_s+TOV,Nsources); 
        x_Vec = [first_segment; signal]; % signal with N_filt\ + Ns_O samples
        first_segment = x_Vec(end-Nseg+1:end,:);
        if nItt == 1
            RMS_x_CZ = rms(x_Vec,1);
        end
        x_Vec  = x_Vec./RMS_x_CZ;
        
        %% Adding RFI signal: tone sine waveform
        if enbl_RFI 
            fprintf('Adding RFI source... \n')
            tic; 
            tau = ones(size(x_Vec,1),1) .* ...
                                          [cosd(RFI_Elev)*sind(RFI_Az),...
                                           cosd(RFI_Elev)*cosd(RFI_Az),...
                                           sind(RFI_Elev)] * Ant_pos.'/c;
            %Sample-Times for the Delayed/Advanced Sequence
            SI_Vec = transpose(int64(round((T_s)*F0).' + ...
                              (0 : 1 : size(x_Vec,1)-1)));
            ST = double(SI_Vec)/F0; 
            RFI_sig = tpm_RFI(RFI_freq,RFI_amp,ST-tau); 
        end

     %% Set the RMS and add intermodulation product and quantize the signal
        x_Vec  = x_Vec * RMS_ADC_bit;     
        fprintf('Adding HDs&IMDs... \n')
        x_Vec = x_Vec + ((x_Vec.^2 * a2) + (x_Vec.^3 * a3)) * IMDGain; 
        
        if enbl_RFI 
            x_Vec = RFI_sig + x_Vec;
        end
%           RFI_sig  = RFI_generator(IMDGain);
%         x_Vec1 = RFI_sig ;
%             x_Vec2 = RFI_sig2 ;
%  
%             x_orig1 = cross_spect(x_Vec1(:,1), x_Vec1(:,1),Analysis_full);
%             x_orig2 = cross_spect(x_Vec(:,1), x_Vec(:,2),Analysis_full);    
%             
%             semilogy(N/1e6,abs(x_orig1),'-',N/1e6,abs(x_orig2));
        if enbl_samples
            x_Vec_q = quantization(x_Vec,samples_levels);
        else 
            x_Vec_q = x_Vec;
        end
        toc;
        
        %% Polyphase filterbank
        fprintf('Processing the Signals with Coarse Channelizer...')
        tic; 
        CCx = zeros(nTimes, Nc/2,Na);
        CCxq = zeros(nTimes, Nc/2,Na);
        for i=1:Na
            CCx(:,:,i)  = pfb_real(x_Vec(:,i),filt_coeff,Nc/2,ovs);
            CCxq(:,:,i) = pfb_real(x_Vec_q(:,i),filt_coeff,Nc/2,ovs);
        end
         CCx(end,:,:) = [];
         CCxq(end,:,:) = [];
        [Nsample,Ncc] = size(CCx(:,:,1));
        toc;
        
        %% FFT equalizer   
        fprintf('Equalizing the Signals...')
        tic;
        % Compute normalization and calibration coefficients
        % norm_curve(Ncc,Na): average RMS amplitude level
        % fft_eq: bits to discard in FFT equalizer (same for all antennas)
        % cal_coef: coefficient to normalize output to (RMS_eq*2^11)
        % gain_mean: mean amplitude gain for all antennas
        % gain_corr(Ncc,Na): Gain correction to apply
        % denormalization: factor to come back to calibrated data 
        % norm_fact(Ncc,Na): Normalization/calibration factor applied

        if nItt == 1
            norm_curve = squeeze(rms(CCxq,1));
            norm_mean  = mean(norm_curve,2);
            gain_tot = gain(:,1:Na) ./ RMS_x_CZ ; % amplitude gain
            
             %% gain parameter is not defined from 1 to 13 channel
            %this line is necessary in order to not have for non-defined
            %channels a NaN element in the final result
             gain_tot(abs(gain_tot) == 0) = eps+1i*eps;
            gain_tot_mean = mean(abs(gain_tot),2);
            gain_mean = mean(abs(gain(:,1:Na)),2);
            % FFT equalization, in bits to shift
            fft_eq = ceil(log2(max(norm_curve,[],2)./RMS_eq));
            fft_eq(fft_eq < 0) = 0;
            
            norm_fact =  round((RMS_eq .*2^11.*(gain_tot_mean./gain_tot.*...
                         2.^fft_eq ./norm_mean))) ./ (2^11 * 2^(eq_bit-beam_bit) );
             
            % 2^11 * 2^(eq_bit-beam_bit)  =    
            %                               2^7 for 16-bit quant.
            %                               2^11 for 12-bit quant. 
            %                               2^15 for 8-bit quant. 
                     
            denormalization = mean(RMS_x_CZ,2).*2^(eq_bit-beam_bit)./ ...
                             (mean(RMS_eq./(norm_mean),2).*RMS_ADC_bit);
           
        end
        % Rescaling at FFT output
        CCx_eq = CCx./(2.^fft_eq');
        CCxq_eq= CCxq./(2.^fft_eq');

        if enbl_eq
            CCxq_eq = quantization(CCxq_eq,eq_levels);
        end 

        toc;
        
        %% Phase Delay Correction  
        fprintf('Delay and Phase correction...')
        tic; 
        f =(F0/2^(10)) * (0: 2^(9) -1).';

        %Sample-Times for the Delayed/Advanced Sequence
        SI_Vec = transpose(int64(round((T_s)*F1).' + (0 : 1 : Nsample-1)));
        ST = double(SI_Vec)/F1;
        HA = beam_HA0 + ST*HA_rate; 
        %The Time variable Delay in sec for the Time Duration
        TVD_Vec = -( [cosd(beam_DEC)*cosd(HA),...
                    -cosd(beam_DEC)*sind(HA),...
                    sind(beam_DEC)*ones(Nsample,1)]*d_Vec/c);

        % Applying delays 
        for z =1:Na
            Phase_sft = exp(-2j*pi*TVD_Vec(:,z).*f');%-
            CCx_eq(:,:,z) = (CCx_eq(:,:,z) .* Phase_sft);
            CCxq_eq(:,:,z) =(CCxq_eq(:,:,z).* Phase_sft);
        end                  
        toc;

        %% Calibration   
        fprintf('Calibration process...')
        tic; 

        CCx_cal = zeros(Nsample,Ncc,Na);
        CCxq_cal = zeros(Nsample,Ncc,Na);

        for i =1:Na
            CCx_cal(:,:,i)  = CCx_eq(:,:,i) .*norm_fact(:,i).'; 
            CCxq_cal(:,:,i) = CCxq_eq(:,:,i).*norm_fact(:,i).'; 
        end

        if enbl_beam
            CCxq_cal = quantization(CCxq_cal,beam_levels);
        end
        toc;
        clear 'CCx_eq' 'CCxq_eq'
        
        %% Beamforming
        CCx_beam  = squeeze(sum(CCx_cal,3));
        CCxq_beam = squeeze(sum(CCxq_cal,3));
        
    %% Correct for bandpasses and normalize 
        fprintf('Generating the composite spectrum...')
        tic; 
        XC1_t = composite_spectrum(CCx_cal, Analysis, denormalization, ...
                                  gain_mean.', Polyphase.H2D);
                              
                                
        XCq1_t= composite_spectrum(CCxq_cal, Analysis, denormalization, ...
                                  gain_mean.', Polyphase.H2D);
                       
        XC1 = XC1 + (XC1_t);
        XCq1 = XCq1 + (XCq1_t);
        
       % Correction for Poliphase filterbank factor (2^15) 
        XC1_v = XC1_v + (XC1_t*2^30).*conj(XC1_t*2^30);
        XCq1_v = XCq1_v + (XCq1_t*2^30).*conj(XCq1_t*2^30);   
        
        XC2_t =composite_spectrum(CCx_beam, Analysis, denormalization,  ...
            mean(abs(gain(:,1:Na)),2).', Polyphase.H2D);
        XCq2_t=composite_spectrum(CCxq_beam, Analysis, denormalization, ...
            mean(abs(gain(:,1:Na)),2).',Polyphase.H2D);
                 
        XC2 = XC2 + (XC2_t);
        XCq2 = XCq2 + (XCq2_t);

        XC2_v = XC2_v + (XC2_t/Na^2*2^30).*conj(XC2_t/Na^2*2^30);
        XCq2_v = XCq2_v + (XCq2_t/Na^2*2^30).*conj(XCq2_t/Na^2*2^30);   
        toc;
        clear 'CCx_cal' 'CCxq_cal'
    end %end for
    
         %% Saving results and normalize
    fprintf('Saving the Results...')
    tic; 
    
    std_xc1  = sqrt((XC1_v-(XC1.*conj(XC1))/NItt)/(NItt-1)/NItt); %#ok<*NASGU>
    std_xcq1 = sqrt((XCq1_v-(XCq1.*conj(XCq1))/NItt)/(NItt-1)/NItt);
    std_xc2  = sqrt((XC2_v-(XC2.*conj(XC2))/NItt)/(NItt-1)/NItt);
    std_xcq2 = sqrt((XCq2_v-(XCq2.*conj(XCq2))/NItt)/(NItt-1)/NItt);
    
     % Correction for the number of antennas (Na^2), for Poliphase 
     % filterbank factor (2^15) and for number of iterations              
    XC1      = XC1/NItt*2^30;
    XCq1     = XCq1/NItt*2^30;
    XC2      = XC2/NItt/Na^2*2^30;
    XCq2     = XCq2/NItt/Na^2*2^30;
    
   filename = ['LFAA_' num2str(Na) 'Ant_bit' num2str(samples_bit) '-' ...
       num2str(eq_bit) '-' num2str(beam_bit) '_sigma_'  num2str(s1) '_RFI_'...
       num2str(RFI_amp(1)) '_T_' num2str(n_ms) 'ms.mat' ];

    save(filename,'XC1_v', 'XCq1_v','nItt', ...
    'XC1', 'XCq1','std_xc1','std_xcq1',...
    'XC2', 'XCq2','std_xc2','std_xcq2', ...
    'ovs','RFI_freq','RMS_eq','Na','F0','n_ms', ...
    'RMS_x_CZ','Na');
    toc;

end %end function