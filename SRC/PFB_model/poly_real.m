function x = poly_real(inputSamples, filt, nChans, overSampling)
%% POLY_REAL - A polyphase filterbank function with real input
%
% The function simulates the PFB with overlapping between input and output:
% each lenFrame samples coming into the filter, frameOf filtered samples go
% out. The function compensates the phase shift due to overlapping,
% rotating the output samples by nof sample. In this way, the following 
% FFT time origin is realigned with the filter input samples.
%
% Syntax:  x = poly_real(inputSamples, filt, nChans, overSampling)
%
%% Inputs:
%   inputSamples        Single column real sample vector
%   filt                Coefficient filter, size 2n*k
%   nChans              Number of output channels
%   overSampling        oversampling factor, default is 1
%
%% Outputs:
%   x                   (nTimes, frameOf)  with the output filtered signal 
%                       divided in frameOf samples, due to the overlapping
%                       between input and output. Each row is a consecutive
%                       frame.
%
%
% See also: PFB_REAL

%% Author: G.Comoretto
% INAF - OAA - Florence
% email address: comore@arcetri.astro.it  
% May 2014; Last revision: 26-May-2014

%% ------------- BEGIN CODE --------------

% Low-pass filtering function with overlapping between input and output:
% each lenFrame samples input, frameOf filtered samples output.

na = size(inputSamples,1);              % Recover size of input

if  nargin == 4
    nChansOvs = nChans/overSampling;	% n1 is oversampled output period
else
    nChansOvs = nChans;                 % Default: oversampling = 1 (no ovs)
end

nChans2=2*nChans;
nOverlap=nChans2-2*nChansOvs;           % Overlapping channels
nFilter = size(filt,1);                 % Size of prototype filter
nSegment = nFilter/(nChans2);           % Number of segments in filter
nTimes = floor((na-nFilter)/nChansOvs/2)+1;
x=zeros(nTimes, nChans*2);              % Output matrix: rows=time, cols=freq
i0=1;                                   % Starting point in input data
j=1;                                    % Index of output row (time)
t0=0;                                   % for time rotation

%% Loop over time segments.
% Each time segment is an FFT of 2n points taken on nf points convolved with the filter
% Successive segments are separated by 2n/ovs = 2*n1 points

while i0+nFilter-1 <= na
    t = sum(reshape(inputSamples(i0:(i0+nFilter-1),1).*filt, ...
                    [nChans2,nSegment]),2);
    % Rotate samples to compensate for overlap phase shift
    % Comment if it is performed in the frequency domain
    t = circshift(t,[t0, 0]);     % Rotate FFT input,compensate for freq. rot.
    t0 = mod(t0 + nChansOvs*2 ,nChans2);
    
    % End rotate samples
    
    x(j,:)=t;
    i0 = i0+nChansOvs*2;
    j = j+1;
end