function [freq_ip3,amp_ip3]= compute_3impbis(rfi_in)
%% compute_3imp - Compute the 3rd intermodulation products of a list of lines
%
% Input and output in dB, for nominally IP3=0dB
% Output is sorted in order of increasing frequency, 
% with duplicates summed together
%
% Syntax:  [freq_ip2,amp_ip2]= compute_2imp(rfi_in)
%
%% Input
% rfi_in:  nx3 table of lines. 
%	Column 1 is frequency,
%	Column 2 is input amplitude (dB),
%	Column 3 is gain (dB)
%
%% Output: 
% freq_ip3: list of frequencies for intermodulation products
% amp_ip3:  list of amplitudes at output (dB)
% 
%% Author: S.Chiarucci
% INAF - OAA - Florence
% email address: simchi@arcetri.astro.it  
% Jan 2019 - Last revision: 05-03-2019
%% Changes
% S. Chiarucci March 2019

%% ------------- BEGIN CODE --------------

   k = size(rfi_in,1);
   freq=zeros(k,k,k);
   amp_i = zeros(k,k,k);
   amp_rfi_in =   10.^(rfi_in(:,2)/20);
   IP3 = -6.322;
   c3 = 4/3/10^(IP3/10);
   ind=0;
   addfreq = zeros(nchoosek(length(rfi_in(:,1)),3),2);

   
   for i = 1:k
     for j = i:k
        for z = j:k
            if i == j
                if (j == z)  
                    freq(i,i,i) = 3*rfi_in(i,1);
                    amp_i(i,i,i) = 1/4* c3 * amp_rfi_in(i)^3;
%                     fprintf('3f%i\n', i)
                else
                    freq(i,i,z) = 2*rfi_in(i,1) + rfi_in(z,1);
                    freq(z,z,i) = 2*rfi_in(z,1) + rfi_in(i,1);
                    
                    amp_i(i,i,z) = 3/4* c3 * (amp_rfi_in(i)^2 * amp_rfi_in(z)); 
                    amp_i(z,z,i) = 3/4* c3 *(amp_rfi_in(z)^2 * amp_rfi_in(i)); 
%                    fprintf('2f%i + f%i\n', i ,z )
%                    fprintf('2f%i + f%i\n', z ,i )
                end
                
            elseif  (i ~= z) && (j ~= z)
                freq(i,j,z) = rfi_in(i,1) + rfi_in(j,1) - rfi_in(z,1);
                freq(i,z,j) = rfi_in(i,1) + rfi_in(z,1) - rfi_in(j,1);
                freq(j,z,i) = rfi_in(j,1) + rfi_in(z,1) - rfi_in(i,1);
                
                ind = ind+1;
                addfreq(ind,1) = rfi_in(i,1) + rfi_in(j,1) + rfi_in(z,1);
                
                amp_i(i,j,z) = 3/2*c3*(amp_rfi_in(i)*amp_rfi_in(j)*amp_rfi_in(z));
                amp_i(i,z,j) = amp_i(i,j,z);
                amp_i(j,z,i) = amp_i(i,j,z);
                addfreq(ind,2) = amp_i(i,j,z);
%                 fprintf('f%i + f%i -f%i\n', i, j, z )
%                 fprintf('f%i + f%i -f%i\n', j, z ,i )
%                 fprintf('f%i + f%i -f%i\n', i, z ,j )      
%                 fprintf('f%i + f%i +f%i\n', i, z ,j )
            else %(i or j are == z)
                freq(i,j,i) = 2*rfi_in(i,1) - rfi_in(j,1); 
                freq(i,j,j) = 2*rfi_in(j,1) - rfi_in(i,1);
                
                amp_i(i,j,i) = 3/4*c3*(amp_rfi_in(i)^2 * amp_rfi_in(j)); 
                amp_i(i,j,j) =  3/4*c3*(amp_rfi_in(j)^2 * amp_rfi_in(i)); 
%              fprintf('2f%i - f%i\n', i ,j )
%              fprintf('2f%i - f%i\n', j ,i )
            end
        end
     end
   end
   
%% Sort lines in order of increasing frequency
   freq = reshape(abs(freq),[],1);
   freq(freq==0) =[];
   freq = [freq ; addfreq(:,1)];
   [~,i]=sort(freq);
   f=freq(i);
   
   amp1 = reshape(amp_i,[],1);
   amp1(amp1==0) = [];
   amp = [amp1; addfreq(:,2)];
   a = amp(i,:).^2;
   
   % Elminate duplicates, summing together power for lines with same frequency
   j=1;
   k = max(f);
   for i = 1:k
    ind = find(f(:) == i);
    if  isempty(ind)
        continue
    end
    freq_ip3(j,1) = i;
    a1(j,1) = sum(a(ind(1):ind(end),1));
    j=j+1;
   end
   
% Copy back results
   amp_ip3 = 10*log10(a1);
   
%     plot(freq_ip3,amp_ip3+40.815,'+')
%     grid on
%     hold on
%     load('RFI_HD_IMD_AAVS1.mat', 'rfi_out')
%     plot(rfi_out(:,1),rfi_out(:,2),'x')
    
   filename = 'Pol_receiver_IMD.mat';
   save(filename,'freq_ip3','amp_ip3','-append');