function x = pfb_real(sample, filt, n, ovs)
%% PFB_REAL - polyphase filterbank with real input
%
% Syntax:  x=pfb_real(sample, filt, n, ovs)
%
%% INPUTS:
% sample: single column real sample vector, arbitrary length
% filt: Coefficient filter, size 2n*k, k integer
% n: Number of output channels (real frequencies only)
% ovs: oversampling factor, default is 1
%
%% OUTPUTS:
%    x: a matrix, with n colums for frequency and rows for time
%
%% Other m-files required: 
%  poly_real.m

%% Author: G.Comoretto
% INAF - OAA - Florence
% email address: comore@arcetri.astro.it  
% May 2014; Last revision: 26-May-2014

%% ------------- BEGIN CODE -------------- 
  x=poly_real(sample,filt,n,ovs)./2^15; 
  x=fft(round(x),[], 2);
  x=x(:,1:n)*2;
end