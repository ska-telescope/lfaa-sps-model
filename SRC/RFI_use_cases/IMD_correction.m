%% IMD_corrrection 
% This script reads the intermodulation products and 
% harmonics values in the second tab (preADU2.0) of the xlsx file and
% selecting the max value of power for each frequency erasing multiple 
% entries.
% The selected values are saved in a new file.
%
%% Author: S.Chiarucci
% INAF - OAA - Florence
% email address:  simchi@arcetri.astro.it
% Sept 2018; Last revision: 02/10/18

%% ------------- BEGIN CODE --------------

num = xlsread('RFI&HD&IMD_AAVS1_ver1.1_old.xlsx',2,'K:L');
j = 1;
for i = 1:807
    ind = find(num(:,1) == i);
    if  isempty(ind)
        continue
    end
    vec(j,1) = i;
    vec(j,2) = max(num(ind(1):ind(end),2));
    j=j+1;
end
xlswrite('RFI&HD&IMD_AAVS1_ver1.1.xlsx', vec,1,'F1')
        