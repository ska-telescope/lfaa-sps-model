function  x_IMD_HD = RFI_generator(Tau)

[ns, na] = size(Tau);
x_rfi = zeros(ns, na);
GaindB = 41;
Gain = 10^(GaindB/20);
ip2 = 4.683-0.7;
ip3 = -6.321+0.4;


% ip2 = 7.183;
% ip3 = -3.321;
  
  ip2_mW = 10^(ip2/10);
  ip3_mW = 10^(ip3/10);
  a2 = 1 / sqrt(ip2_mW)/sqrt(2);
  a3 = 4/3/ip3_mW/2;
  
  load('RFI_HD_IMD_AAVS1.mat','rfi_in');
  rfi_dBm = rfi_in(:,2);
  rfi_amp = sqrt(2)* 10.^(rfi_dBm/20);
  rfi_freq = rfi_in(:,1) .* 1e6;
  
  for i = 1 : length(rfi_in)    
      fprintf('Adding RFI %d of %d\n', i,length(rfi_in))
      
      integ = floor((rfi_freq(i)* Tau));
      fract = (rfi_freq(i)* Tau)-integ;
      x_rfi = x_rfi + rfi_amp(i)* cos(2.0 * pi * fract);
  end
  
  x_IMD_HD = ((x_rfi.^2 * a2) + (x_rfi.^3 * a3))  * Gain;%* (1000/(200*1e-3))^0.25;* (17.5/10^(-2.7/20))