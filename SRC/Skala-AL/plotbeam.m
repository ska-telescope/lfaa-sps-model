el=pi/6;   % 60 degrees
freq=110;  % MHz
taper = 0; % dB of taper at station border
xs=-90:0.5:90; % Map coordinates in degrees
ys=-90:0.5:90;
nant =size(pos,1);
npoints = 180;

[map,rx,ry] = response_polar(pos,el,azimuth,npoints, freq, ones(nant,1), taper);
load('Beam_avg.mat');
prim = sqrt(Beam_110MHz_X(rx,ry));
map = map .* prim;
lmap = 20*log10(abs(map));   % in dB
max1 = max(max(lmap));
lmap = lmap - max1;          % normalize at peak
figure
contour(rx,ry,lmap,-25:5:-10) % contours at -20,-15 & -10 dB
figure
colormap jet
surface(rx,ry,lmap,'FaceColor','interp','EdgeColor','none')
colorbar
caxis([-50 0])