function plotresponse_oversampled(h, Nc, Os)
%% PLOTRESPONSE_OVERSAMPLED - Plot response for oversampled polyphase filterbank
% Plots stopband, channel bandshape and passband ripple 
%
% Syntax:  plotresponse_oversampled(h, Nc, Os)
%
%% INPUTS:
% h  = filter tap coefficients
% Nc = number of channels (critically sampled)
% Os = oversampling factor (for display of aliased response)
%
%% OUTPUT:
%    none. Plot with 3 subplots: 
%     - complete transfer function, with emphasis on the stipband
%     - single channel, with direct and aliased response 
%     - passband ripple response
%
% Other m-files required: fft_ir.m 
%

%% Author: G.Comoretto
% INAF - OAA - Florence
% email address: comore@arcetri.astro.it  
% June 2018; Last revision: 20/06/18

%% ------------- BEGIN CODE --------------

   % Compute power response of filter with 2^20 points
   len =2^20;	     		% length of the display FFT
   spt = fft_ir(h,len/2).^2 ;   % Power response
   nresp=rms(spt(1:512));
   spt= spt/nresp;
   lspt=10*log10(spt);		% response in dB
   f=(0:len/2-1)/len*Nc;	% frequency scale (channels)
   figure; 
   % Plot 1 : Stopband. Goal is -60 dB
   subplot(3,1,1);
   hold on;

   plot(f,lspt);
   fc = 0.5:0.5:(Nc/2);
   rc = -60 -10*log10(fc/0.5);
   rc0 = rc(size(fc,2)/2);
   rc(fc>Nc/4)= rc0;
   %plot([1 Nc/2], [-60 -60],'k--','LineWidth',1);
   plot(fc,rc,'k--','LineWidth',1);
   hold off;
   axis([0 Nc/2 -110 -55]);
   title('Transfer Function of the Prototype Filter - Stopband')
   grid on; box on;
   % Plot 2: bandpass
   subplot(3,1,2);
   hold on;
   plot(f,lspt,Os-f,lspt);
   plot([Os-0.5 Nc/2], [-60 -60],'k--','LineWidth',1);
   plot([0 0.5], [0 0],'k--','LineWidth',1);
   hold off;
   axis([0 1.5 -70 4])
   title('Channel shape')
   legend('Direct','Aliased')
   grid on;box on;
   % Plot 3: passband ripple. Goal +/- 0.2 dB
   subplot(3,1,3)
   hold on;
   plot(f,lspt);   
   plot([0 0.5], [-0.15 -0.15],'k--','LineWidth',1);
   plot([0 0.5], [0.15 0.15],'k--','LineWidth',1);
   hold off;
   axis([0 0.52 -0.22 0.22])
   title('Passband ripple')
   grid on;box on;   
end