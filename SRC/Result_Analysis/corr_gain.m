function g = corr_gain(rho,nbit)
%% CORR_GAIN- Correlator gain for n bit quantiser making use of rngain function
% Quantized correlation gain for a number of
% quantization levels nbit (array or single value) 
% and rho (array or single value)
%
% Syntax: g = corr_gain(rho,nbit) 
%
%% Inputs:
%  nbit = number of quantization levels
%  rho = correlation coefficient
%
%% Outputs:
% g = quantized correlation gain
%
%% Other m-files required: 
%  rngain.m
%
%% Author: S.Chiarucci
% INAF - OAA - Florence
% email address:  simchi@arcetri.astro.it 
% 04-Feb-15; Last revision: 20/06/18

%% ------------- BEGIN CODE --------------

    a=size(rho);
    b=size(nbit);

    g=zeros(100,a(2),b(2));

    for j=1:b(2)
        for i=1:a(2)
            g(:,i,j)=rngain(rho(i),nbit(j));
        end
    end
    return
    
end