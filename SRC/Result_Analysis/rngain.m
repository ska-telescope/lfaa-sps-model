function g = rngain(rho,n)
%% RNGAIN - Gain of quantized signal with n equispaced making use of rn and rnp functions
% Gain of quantized signal for a number of
% quantization levels nbit (single value) 
% and rho (single value)
%
% Syntax:  g = rngain(rho,n)
%
%% Inputs:
% n = number of quantization levels
% rho = correlation coefficient  
%
%% Constrains
%
% -1<rho<1 
% n can be even or odd
%
%% Outputs:
% g = gain of quantized signal 
%
%% Other m-files required:
%  rn.m, rnp.m
%

%% Author: S.Chiarucci
% INAF - OAA - Florence
% email address:  simchi@arcetri.astro.it 
% June 2018; Last revision: 20/06/18
%
%% Changes
%
%  Author            Date           Comments
%  ---------------   -----------
%  ---------------------------------------
%  S.Chiarucci      4-Feb-15   Original version
%  S.Chiarucci      11-Aug-17  Bug correction 
%  ------------------------------------------------------------------------
%

%% ------------- BEGIN CODE --------------
corr_analog=zeros(1,100);
corr_qnt=zeros(1,100);
g=zeros(1,100);

    for i=1:100       
        if mod (n,2) ==1
            sgm=(n-1)/200;
            l=1/sgm;
            corr_analog(i)=rho*((sgm*i)^2);
            corr_qnt(i)=rn(l/i,l/i,rho,n);
        else
            sgm=(n)/200;
            l=1/sgm;
            corr_analog(i)=rho*((sgm*i)^2);
            corr_qnt(i)=rnp(l/i,l/i,rho,n);
        end
        g(i)=corr_qnt(i)/corr_analog(i);
    end
    return
end